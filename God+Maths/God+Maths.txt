﻿1 Invenção ou descoberta?
1.1 A questão dos pirahã (~2000s)

Tem uma tribo no meio da Floresta Amazônica que deu muito o que falar no começo do milênio. Os pirahã.
É que a sua cultura é bem peculiar - eles vivem sobretudo o agora. Então os antropólogos tinham muito o que falar.
Essa parte da cultura deles tinha lá suas muitas consequências. E elas se refletiam na sua língua:
Eles não têm palavras pra tempo. Não se fala do passado muito distante, ou do futuro distante. Fala-se do passado imediato e do futuro imediato.
Eles também não têm palavras pra várias coisas abstratas. Não existem palavras pra cores: são sempre no comparativo. É como o sangue, é como a folha.
E, uma coisa muito interessante, eles também não têm palavra nenhuma pra números. Eles não sabem contar.

Pra lidar com quantidades ou tamanhos, eles têm três palavras. Uma delas significa pequeno, outra grande, e outra muito.
No começo, achava-se que as palavras significam realmente um, dois e além de dois. Mas os antropólogos foram investigando e descobriram que não é bem esse o caso.
O experimento era mais ou menos assim: eles iam estendendo dez fios, um a um, na frente de algum pirahã e pediam pra ele dizer "quantos" fios haviam ali.
Nesse caso, eles sempre usavam pequeno pra um, grande pra dois e muito pra mais que isso.
Mas daí eles invertiam. Haviam dez fios estendidos e eles iam tirando.
Nesse caso, variava bastante. Todos começavam com muito. Alguns usavam grande a partir de uns sete. Aos três, todos falavam pequeno.
Então sabemos que essas palavras são mais noções comparativas, dependentes de contexto; e não números.

Na verdade, fizeram outra série de experimentos que mostraram que os pirahã têm dificuldade com problemas de contagem exata.
O avaliador fazia de um a cinco riscos num lado de uma folha e o pirahã tinha de fazer a mesma quantidade de riscos ao lado.
Pra um ou dois, era ok. Mas a partir de três, eles se perdiam.

Os antropólogos e linguistas então teorizam que a certa altura da História os seres humanos eram como os pirahã, nesse sentido.

Mas então... como surgiu essa ideia, de contar?

-- Suméria: A invenção dos números (e dos contadores)
--- Bijeção de grupos: Ovelhas e contagem

Uma das civilizações mais antigas que sabemos conhecerem o conceito de número são os sumérios.

Aparentemente, quando um sumério tinha gado, ele carregava consigo uma pequena sacola com peças, uma peça para cada cabeça de gado.
Quando ele negociava alguns de seus bois e vacas e os dava a alguém, entregava também o mesmo número de peças, uma para cada cabeça.
Essas pecinhas, por algum motivo, de vez em quando eram seladas em vasos de barro.
Eles colocavam uma quantidade de peças num vaso, então lacravam e marcavam por fora um símbolo pra se lembrar quantas peças estavam lá dentro.
Não demorou muito pra alguém ter a ideia de, ao invés de ter pecinhas em vasos com marquinhas, simplesmente fazer as marquinhas em algum lugar, representando tantas peças.
E, pra assegurar que esses símbolos representavam fielmente alguma quantidade de produtos - fossem gado, trigo ou qualquer outra coisa - surgiram os contadores reais.
Os contadores são, então, uma das profissões mais antigas deste mundo.

-- A teoria dos conjuntos (a matemática intrínseca à Criação)

	A linguagem matemática é inventada. As coisas matemáticas, não.

- Os Dois Livros
-- Johannes Kepler
--- Biografia
--- Astronomia
--- Contexto (ciência em seus primórdios)
--- Teologia
-- A Lei de Kepler
-- As Harmonias do Mundo, de Johannes Kepler
	O, Almighty God, I am thinking Thy thoughts after Thee!...
	The book is written, to be read either now or by posterity, I care not which.
	It may be well to wait a century for a reader, as God has waited six thousand years for an observer.
-- Libri Naturae
-- Ramanujan
	An equation means nothing to me unless it expresses a thought of God.

- A curiosidade da Criação
-- Números naturais e o infinito
-- Atravessando dimensões: A curva de Hilbert
-- A questão dos fractais

- Fractais
-- Definição (semelhança invariante com escala)
-- Exemplos: (naturais) espirais, encostas, copas de árvores, dunas
-- gifs, muitos deles!
-- Exemplos: (construídos) Koch, Hilbert, (Cantor?)
-- Exemplos: (auto-semelhança) Sierpinski, Tapete, Esponja

- Deus fractal?

- Mandelbrot
-- Números complexos
-- Definição de Conjunto de Mandelbrot
-- Exemplos
-- Explorando o conjunto de Mandelbrot

- Conclusões finais

- A oração de Kepler
I thank You, my Creator, that You have given me joys in Your creation and ecstasy over the work of Your hands.

I have known the glory of Your works as far as my finite spirit was able to comprehend Your infinity.
If have said anything wholly unworthy of You, or have aspired after my own glory, graciously forgive me.

If I have been enticed into brashness by the wonderful beauty of thy works,
or if I have loved my own glory among men,
while advancing in work destined for thy glory,
gently and mercifully pardon me:
and finally, deign graciously to cause
that these demonstrations may lead to thy glory and to the salvation of souls,
and nowhere be an obstacle to that. Amen.

Holy Father, keep us safe in the concord of our love for one another,
that we may be one just as Thou art with Thy Son, Our Lord, and with the Holy Ghost,
and just as through the sweetest bonds of harmonies Thou hast made all Thy works one,
and that from the bringing of Thy people into concord,
the body of Thy Church may be built up in the Earth,
as Thou didst erect the heavens themselves out of harmonies.