\section*{Tornando Prático: Confiando em Deus enquanto Economizo}

Nosso medo e ansiedade em relação ao dinheiro se refletem mais diretamente em
nossas finanças através de como economizamos e lidamos com a dívida. Economizamos
dinheiro agora porque desejamos usar esses recursos (mais juros) no futuro.
Curiosamente, a dívida pode ser vista da mesma maneira: pagamos as dívidas agora,
porque desejamos ter menos obrigações financeiras (principal e juros) no futuro.
Em outras palavras, o pagamento da dívida ajuda alguém a passar de um patrimônio
líquido negativo para zero, enquanto a economia ajuda alguém a passar de um
patrimônio líquido zero para positivo. São dois lados da mesma moeda (com
trocadilho). Muitas vezes, fazemos ambos ao mesmo tempo na vida, como economizar
para a aposentadoria enquanto pagamos empréstimos estudantis.

Como economizar dinheiro com sabedoria é uma área complicada de avaliar do ponto
de vista espiritual. Por um lado, as Escrituras afirmam que economizar dinheiro e
pagar dívidas é sábio.\footnote{Pv 6:6-8; 22:7.} Por outro lado, somos ordenados a
não ser acumuladores de riquezas.\footnote{Ec 5:13.} Então, como devemos lidar com
economias e dívidas?

A falta de economias pode indicar sabedoria ou insensatez, dependendo das
circunstâncias. Se você economiza pouco porque gasta cada dólar que ganha em bens
de consumo, a Bíblia o chama de ``insensato''.\footnote{Lc 12:13-21.} Se você
economiza pouco porque está seguindo corajosamente o chamado claro de Deus em sua
vida, por exemplo, como missionário com um salário baixo, está acumulando tesouro
no céu ao servir ``os pequeninos''.\footnote{Mt 25:31-46.}

Da mesma forma, uma taxa alta de economias pode indicar sabedoria ou insensatez,
dependendo das circunstâncias. Se você economiza diligentemente para pagar suas
dívidas e preparar uma entrada para uma casa acessível... isso é
sabedoria.\footnote{Pv 21:20.} Se você economiza diligentemente porque ama
acumular riquezas para si mesmo e busca uma aposentadoria precoce e tranquila...
isso é insensatez.\footnote{Lc 12:13-21.}

Infelizmente, a maioria das pessoas não economiza muito. Nos Estados Unidos, os
1\% mais ricos economizam aproximadamente 40\% de sua renda, os 10\% mais ricos
economizam aproximadamente 10\% de sua renda, e todas as outras pessoas,
estatisticamente, não economizam nada.\footnote{Emmanuel Saez and Gabriel Zucman,
\textit{Savings Rates by Wealth Class} graph, citado em \textit{The Average
Savings Rates by Income (Wealth Class)}, Financial Samurai, acessado em 9 de
agosto de 2018,
https://www.financialsamurai.com/the-average-savings-rates-by-income-wealth-class/.\footnotemark}
\footnotetext{No caso do Brasil, dados jul/2018 revelaram que nas classes A e B
cerca de 38,6\% haviam poupado alguma coisa, enquanto nas classes C, D e E apenas
12,8\% haviam poupado alguma coisa. Considerando apenas as pessoas que poupam, a
média mensal poupada fica em torno de R\$ 500. SPC Brasil, \textit{Indicador de
Reserva Financeira - Julho/2018}, acessado em 27 de junho de 2023,
https://www.spcbrasil.org.br/wpimprensa/wp-content/uploads/2018/08/An\%C3\%A1lise\_-Indicador-de-Reserva-Financeira-jul\_18.pdf.}
Independentemente de você estar entre o 1\% mais rico ou os 10\% mais pobres, um
plano claro de economias pode ajudá-lo a administrar com sabedoria os recursos que
Deus colocou em suas mãos.

Economizar envolve três estágios: 1. Criar um colchão; 2. livrar-se das dívidas
ruins; e 3. planejar com antecedência. As economias bíblicas são economias
orientadas por metas, destinadas a suprir nossas necessidades razoáveis. Uma vez
que essas necessidades sejam atendidas e à medida que alcançamos níveis mais altos
de riqueza, nossa necessidade de economizar diminui, e podemos nos concentrar mais
plenamente na generosidade do reino. No entanto, se não progredirmos por esses
três estágios, prejudicamos nossa capacidade de servir a Deus com nossas finanças.

\subsection*{Estágio 1: Criar um Colchão}

Se você tiver menos de US\$ 2.000 em dinheiro imediatamente
disponível,\footnote{Considerando que o salário mínimo nos Estados Unidos é de
cerca de US\$ 1.000 por mês, para esta seção podemos ler como se fossem dois
salários mínimos nossos. (Nota do Editor)} seja em uma conta corrente ou poupança,
um envelope no seu armário ou em qualquer outro lugar, então você está operando
sem um colchão. Um pneu furado, um vazamento no telhado, um susto de saúde
inesperado -- qualquer um desses eventos comuns poderia colocá-lo em uma crise.
Na verdade, um grande estudo descobriu que manter US\$ 2.000 em mãos é uma prática
financeira fundamental que contribui para uma mentalidade financeira melhorada e
um senso de bem-estar.\footnote{Brad Hewitt and James Moline, \textit{Your New
Money Mindset: Create a Healthy Relationship with Money} (Carol Stream, IL:
Tyndale, 2015), 241 (notas do capítulo 2).}

No Estágio 1, acreditamos que é apropriado ter como objetivo economizar de forma
constante e até mesmo agressiva. No entanto, certifique-se de manter sua
generosidade para com Deus, mesmo enquanto constrói essa reserva. Não apenas dar é
um ato de obediência, mas também traz grande alegria que você não quer perder.

Famílias que têm economizado por algum tempo podem ter fundos de emergência muito
maiores do que US\$ 2.000. Idealmente, as famílias economizariam de três a seis
meses de despesas em uma conta corrente ou poupança, permitindo-lhes lidar com
qualquer emergência ou realizar atos espontâneos de generosidade com liberdade e
flexibilidade. Infelizmente, pelo menos 40\% da população americana não completou
o Estágio 1.\footnote{\textit{Report on the Economic Well-Being of U.S. Households
in 2017} (Washington, DC: Board of Governors of the Federal Reserve System, 2018),
https://www.federalreserve.gov/publications/files/2017-report-economic-well-being-us-households-201805.pdf,
21. O estudo também descobriu que 46\% dos lares americanos não conseguem dispor
de US\$ 400 sobrando para uma emergência (21).\footnotemark} \footnotetext{Numa
pesquisa em julho/2018 da SPC Brasil, apenas 12\% dos brasileiros disseram que
conseguiriam arcar com uma despesa significativa inesperada. SPC Brasil,
\textit{Indicador de Bem-Estar Financeiro - Julho/2018}, acessado em 27 de julho
de 2023,
https://www.spcbrasil.org.br/wpimprensa/wp-content/uploads/2018/08/An\%C3\%A1lise-Bem-Estar-Financeiro\_julho-v3.pdf.}
Economizar US\$ 2.000 coloca você em uma posição muito melhor para absorver o tipo
de despesa inesperada e significativa que ocorre para todos nós em algum momento
de nossas vidas.

Portanto, para nossos propósitos, uma vez que você tenha US\$ 2.000 em dinheiro ou
em uma conta corrente ou poupança, você passou para o Estágio 2.

\subsection*{Estágio 2: Livrar-se das Dívidas Ruins}

Ter dívidas significa que você presumiu sobre o futuro para financiar seu estilo
de vida presente. Isso pode ser assustador. Se você tem trinta anos e tem dívidas,
você está dizendo para o seu ``eu'' de trinta e cinco anos: ``Ei, posso pegar
emprestado algum dinheiro? Vou te pagar de volta, prometo!''

Todas as dívidas, exceto hipotecas -- empréstimos de carro, empréstimos
estudantis, saldos não pagos de cartão de crédito e assim por diante -- são
``dívidas ruins''. Elas não são garantidas por um ativo que se valoriza como uma
hipoteca e devem ser pagas o mais rapidamente possível, de forma razoável. Elas
representam uma escravidão em nossas vidas e uma mancha potencial em nosso
legado.\footnote{Pv 22:7. Como escreveu Basílio de Cesareia: ``A dívida de um pai
leva à prisão. Não deixe um vínculo, uma maldição paternal, por assim dizer,
recair sobre os filhos e netos.'' (Homilia sobre o Salmo 14, tradução própria.)}
Idealmente, a única dívida de longo prazo que teríamos é uma hipoteca acessível, o
que geralmente significa que o saldo é o dobro da sua renda ou menos. Por exemplo,
uma família com uma renda de US\$ 100.000 (anuais) deve buscar um valor de
empréstimo de US\$ 200.000 (ao ano) ou menos.\footnote{Uma proporção de 2:1 entre
o empréstimo hipotecário e a renda anual pode ser desafiadora em certas regiões,
especialmente em grandes centros urbanos. Se você mora em uma área assim,
recomendamos buscar aconselhamento de um consultor financeiro cristão confiável
sobre qual tamanho de empréstimo hipotecário é adequado para você, mas saiba que
exceder essa proporção comprometerá uma parcela significativamente grande de sua
renda.}

Uma casa deve atender às necessidades da sua família sem ser excessiva. Devemos
escolher uma casa por motivos de utilidade e necessidade, em vez de status ou
orgulho. Descobrimos que até mesmo cristãos muito maduros, mesmo aqueles que são
extremamente generosos, admitem ter dificuldade em manter a modéstia na compra de
uma casa.

Além disso, lembre-se de que as calculadoras de acessibilidade de casa online
pressupõem que você não é um doador. Tenha cuidado com o que elas dizem. Se você
pegar um empréstimo que excede o dobro da sua renda bruta, provavelmente estará
alavancado a ponto de não poder investir generosamente no reino de Deus e, em vez
disso, será leal principalmente ao ``Reino da Sua Casa''.

\begin{quotation}
\textbf{Exemplo:} Se a sua renda é de US\$ 80.000 e você tem US\$ 50.000
economizados para a entrada, seu orçamento para a compra da casa é de
$\text{US\$ } 50.000 + 2 \times \text{US\$ } 80.000 = \text{US\$ } 210.000$.
\end{quotation}

Também devemos observar que planejadores sábios buscam evitar o endividamento para
um empréstimo de automóvel sempre que possível. Depois de pagar seus carros,
comece a economizar uma quantia mensal para pagar à vista pelo seu próximo carro,
mesmo que a compra esteja a anos de distância. (Observe que essas economias são
adicionais ao seu fundo de emergência -- elas são completamente separadas e podem
ser mantidas em sua própria conta poupança.) Você pode ter que dirigir um carro
velho por um tempo, mas isso compensará a longo prazo.

No Estágio 2, procure destinar de 20 a 40 por cento da sua renda para eliminar
dívidas. (20 por cento pode parecer impossível. Tudo bem começar com um número
menor e aumentar gradualmente.) Assim como no Estágio 1, nunca deixe sua paixão
por eliminar dívidas sufocar seu desejo de ser generoso. Um grande estudo
financeiro das famílias americanas descobriu que o maior inibidor da generosidade
era a dívida.\footnote{Amy L. Sherman, \textit{Directions in Women's Giving 2012},
(Orlando: Women Doing Well, 2012), http://womendoingwell.org/about/research-new/.} À medida que você caminha em direção à liberdade das dívidas, sua
generosidade pode aumentar.

\subsection*{Estágio 3: Planejar com Antecedência}

Se você está livre de dívidas, exceto por uma hipoteca acessível, e tem uma
reserva de US\$ 2.000 ou mais, então, pela graça de Deus, você alcançou uma base
financeira sólida e pode começar a planejar para o futuro de longo prazo. Apenas
cerca de um quarto das famílias americanas estão no Estágio 3.

Quando a maioria das pessoas pensa a longo prazo, pensa em dez, vinte ou trinta
anos no futuro. Isso é sábio. Mas Deus nos convida a estender nosso horizonte
ainda mais. Onde estará o seu legado financeiro daqui a mil anos? Cem mil anos?
Eventualmente (depois de bilhões de anos), até mesmo o sol se apagará. Mas tudo o
que você investiu no reino de Deus está garantido a render dividendos.

Em um experimento, crianças foram colocadas em uma sala com um marshmallow. Elas
poderiam comê-lo se quisessem. Mas se esperassem tempo suficiente sem comê-lo,
seriam recompensadas com dois marshmallows. As crianças que conseguem esperar mais
demonstram maior maturidade, pois entendem o conceito de gratificação adiada. Dois
marshmallows em cinco minutos são melhores do que um marshmallow agora mesmo.

Temos toda a capacidade de ``comer nossos marshmallows'' agora, aqui na Terra.
Podemos gastar e economizar para nós mesmos, ou acumular riqueza para passar para
futuras gerações. Isso é um marshmallow saboroso. Mas Deus colocou uma oferta
melhor na mesa: seja generoso - invista no reino de Deus -- e ele lhe dará
recompensas que duram para sempre, pela eternidade. Um ``zilhão'' de marshmallows!

No Estágio 3, você alcançou estabilidade financeira e agora tem uma escolha a
fazer. Você vai comer os marshmallows agora? Ou vai adiar sua gratificação para o
céu, onde sua recompensa pela generosidade será imensa?

Para deixar claro, é sábio continuar economizando dinheiro neste estágio. Afinal,
você ainda precisa pagar sua hipoteca, e quitar sua casa antecipadamente é uma
ótima ideia. Além disso, a maioria das famílias tem metas sábias que exigem
economias adicionais, como uma aposentadoria segura na velhice ou a educação
universitária dos filhos.

A importância e o foco das economias, no entanto, devem começar a diminuir à
medida que a riqueza aumenta. Você pode começar o Estágio 3 destinando até 40\% de
sua renda para poupança e pagamento da hipoteca. Essa taxa de poupança deve
diminuir ao longo dos anos à medida que você se aproxima da ideia de
``suficiente''. Neste estágio, é extremamente importante ter cuidado para não
permitir que sua paixão por economizar supere sua paixão por dar. Lembre-se de que
Jesus contou a parábola do rico insensato para alertar contra essa questão. Em
algum momento, sua taxa de poupança pode chegar a 0\%, mas sua taxa de doação pode
continuar a aumentar à medida que você assume projetos cada vez maiores para o
reino de Deus.

Nossas famílias estão no início do Estágio 3, e cada uma ainda economiza de 25\% a
35\% de nossa renda. Também doamos de 15\% a 25\% de nossa renda bruta. Mas
buscamos mentores que estão mais avançados no Estágio 3, que pararam de economizar
e agora doam metade ou até mais de suas rendas. Esperamos fazer o mesmo um dia.

O foco do Estágio 3 é a eternidade. Sua vida na Terra passará rapidamente. Deus
lhe deu três metas eternas: servir aos pobres, fortalecer os crentes e salvar os
perdidos (vamos aprender mais sobre essas metas no capítulo 5). Busque essas metas
com todas as suas forças, para a glória de Deus!

\begin{ideabox}
Muitas pessoas acompanham seu patrimônio líquido, mas você já considerou
acompanhar suas doações acumuladas ao longo da vida? Ver o número crescer pode ser
motivador. Será que você poderia doar US\$ 100.000 ao longo da sua vida? US\$ 1
milhão?
\end{ideabox}

\subsection*{Resumo dos Três Estágios para Economizar}

\begin{enumerate}
\item Criar um Colchão
    \begin{itemize}
    \item Economize \$2.000 em dinheiro, conta corrente ou conta poupança.
    \end{itemize}
\item Livrar-se de Dívidas Ruins
    \begin{itemize}
    \item Salde todas as dívidas exceto uma hipoteca razoável.
    \item A hipoteca deve ser menos que duas vezes a sua renda familiar bruta.
    \end{itemize}
\item Planejar com Antecedência
    \begin{itemize}
    \item Comece a doar mais agressivamente que nunca.
    \item Considere terminar de pagar pela sua casa mais cedo.
    \item Considere objetivos de longo prazo, como aposentadoria, faculdade, etc.
    \end{itemize}
\end{enumerate}
