\section*{Fazedor de Felicidade}

Preocupar-se com nosso dinheiro e acumular riquezas é, no melhor dos casos, uma
perda de tempo e, no pior dos casos, prejudicial ao nosso relacionamento com Deus.
Afinal, ninguém nunca viu uma carreta de mudança atrás de um carro funerário -- ou
seja, você não pode levar nada disso com você!\footnote{Randy Alcorn,
\textit{Managing God's Money} (Carol Stream, IL: Tyndale, 2011), 80.} Devemos
aprender a confiar nos planos de Deus para o nosso dinheiro.

O padre da igreja primitiva, Basílio de Cesaréia (c. 330-379 d.C.), escreveu:

\begin{quotation}
E se você tiver apenas um único pão restante e alguém bater à sua porta, traga o
único pão que você tem, segure-o para o céu e diga esta oração, que não é apenas
generosa de sua parte, mas também pede a compaixão do Senhor: ``Senhor, você vê
este único pão e sabe que a ameaça da fome é iminente, mas coloco o seu mandamento
acima do meu próprio bem-estar e, do pouco que tenho, dou a este irmão faminto.
Dê, então, em troca a mim, seu servo, pois também estou em perigo de passar fome.
Conheço a sua bondade e sou encorajado pelo seu poder. Você não retarda a sua
graça indefinidamente, mas distribui seus dons quando quer.''\footnote{Basílio de
Cesareia, em \textit{Em Tempos de Fome e de Seca}.}
\end{quotation}

Basílio confiava plenamente nos planos de Deus, mesmo sabendo que o povo de Deus
\textit{vai} enfrentar tempos difíceis nesta vida. Seu coração havia sido
transformado pela graça de Deus. Compreender a graça de Deus nos concede a
liberdade de sermos generosos com os outros, confiando no provimento final de Deus
e em gratidão por Sua generosidade para conosco.

Colocar nossa confiança em Deus nos capacita a deixar de lado a ansiedade em
relação às nossas próprias necessidades e permite que nossos corações sejam
inclinados ao chamado de Deus para servir aos outros. Infelizmente, muitos de nós
lutamos para servir aos outros financeiramente: a doação nos Estados Unidos tem
diminuído constantemente como porcentagem da renda nos últimos cinquenta anos,
apesar do aumento massivo na qualidade de vida desfrutado pela maioria dos
americanos nesse período. Essas tendências são verdadeiras independentemente da
faixa de renda: famílias que ganham mais de US\$ 150.000 por ano doam a mesma
porcentagem de sua renda que as famílias que ganham menos.\footnote{John Ronsvalle
and Sylvia Ronsvalle, \textit{The State of Church Giving} vol. 27, \textit{Through
2015: Understanding the Times} (Champaign, IL: Empty Tomb Inc., 2017).} Christian
Smith e Hilary Davidson, autores do livro \textit{The Paradox of Generosity},
determinaram que menos de 3\% dos americanos doam 10\% ou mais de sua renda,
levando-os a afirmar que ``a América tem um problema de
generosidade''.\footnote{Christian Smith and Hilary Davidson, \textit{Giving Makes
Us Happy. So Why Do So Few Do It?}, Science of Generosity, 3 de novembro de 2014,
https://generosityresearch.nd.edu/news/giving-makes-us-happy-then-why-do-so-few-do-it/.\footnotemark}
\footnotetext{Quanto ao Brasil, em 2020, 7\% dos doadores disseram doar mais de
R\$ 80 por mês. Se aumentarmos a faixa de corte para R\$ 200 mensais, esse índice
cai para 3\%. \textit{Pesquisa Doação Brasil 2020}, IDIS -- Instituto para o
Desenvolvimento do Investimento Social, acesso em 27 de junho de 2023,
https://pesquisadoacaobrasil.org.br/resultados/. (Nota do Editor.)}

Muitos de nós veem dar dinheiro à igreja como um dever religioso ou obrigação.
Embora Deus nos instrui a dar, a palavra \textit{dever} não poderia ser uma
descrição menos precisa da visão de Deus para a nossa generosidade. Quando se
trata de generosidade, Deus deseja algo \textit{para} nós, não \textit{de} nós!

Jesus nos diz: ``Há maior felicidade em dar do que em receber'' (Atos 20:35). O
autor Randy Alcorn afirmou que a tradução grega da palavra \textit{felicidade}
nesse versículo, \textit{makarios}, pode ser mais literalmente traduzida como
"fazedor-de-felicidade".\footnote{Randy Alcorn, \textit{More Happy-Making to
Give}, Eternal Perspectives Ministries, 29 de março de 2010,
http://www.epm.org/resources/2010/Mar/29/happy-making-video/.} O versículo é mais
literalmente traduzido como ``É mais fazedor-de-felicidade dar do que receber''.
Recebemos uma alegria incrível quando servimos aos outros com nossos recursos
financeiros. ``Dêem, e lhes será dado: uma boa medida, calcada, sacudida e
transbordante será dada a vocês. Pois a medida que usarem também será usada para
medir vocês.''\footnote{Lc 6:38.}

Além disso, nossa generosidade para com os outros neste mundo se traduz na
generosidade eterna de Deus para conosco no céu. ``Vá, venda os seus bens e dê o
dinheiro aos pobres, e você terá um tesouro nos céus''.\footnote{Mt 19:21.} E
``Não acumulem para vocês tesouros na terra, ... Mas acumulem para vocês tesouros
nos céus, ... Pois onde estiver o seu tesouro, aí também estará o seu
coração.''\footnote{Mt 6:19-21. Muito mais pode ser dito sobre recompensas
eternas. Para um estudo fantástico do assunto, veja \textit{The Law of Rewards},
de Randy Alcorn.}

De fato, as Escrituras relacionam diretamente o cuidado com os pobres ao
conhecimento de Deus. ``Se alguém tiver recursos materiais e, vendo seu irmão em
necessidade, não se compadecer dele, como pode permanecer nele o amor de
Deus?''\footnote{1 Jo 3:17.} ``Ele defendeu a causa do pobre e do necessitado...
Não é isso que significa conhecer-me?, declara o Senhor.''\footnote{Jr 22:16.}

Deus afirma que devemos ser generosos não por obrigação religiosa, mas sim porque
ser generoso nos traz uma alegria tremenda. ``Há quem dê generosamente, e vê
aumentar suas riquezas; ... quem dá alívio aos outros, alívio
receberá.''\footnote{Pv 11:24-25. Para esclarecer, este versículo \textit{não
está} apoiando uma teologia falsa do evangelho da prosperidade. Deus promete nos
abençoar se formos generosos, mas essas bênçãos podem não se manifestar na forma
de recompensa financeira. Elas podem ser outras bênçãos aqui na terra, como
relacionamentos mais fortes ou um maior senso de propósito, ou talvez não
experimentemos as bênçãos até estarmos com Deus no céu.} ``Aquele que semeia com
fartura, também colherá fartamente... Deus ama quem dá com alegria.''\footnote{2
Co 9:6-7.}

Deus não precisa do nosso dinheiro. Ele é soberano e, no final, realizará Seus
propósitos. Ao mesmo tempo, Deus escolheu nos dar um papel importante a
desempenhar aqui na Terra. Portanto, a generosidade deve ser vista como uma
oportunidade dada por Deus para caminharmos ao lado Dele no cumprimento do Seu
reino. Que plano para o nosso dinheiro poderia ser maior ou melhor do que isso?
