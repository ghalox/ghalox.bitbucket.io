\section*{Me Compre um Barco}

Os Beatles têm uma famosa música que nos lembra que o dinheiro não pode comprar
amor.\footnote{The Beatles, \textit{Can’t Buy Me Love}, por Lennon--McCartney,
lançado em 16 de março de 1964, Capitol Records, 7 rpm.} Mas o astro da música
country Chris Janson escreveu uma música engraçada chamada ``\textit{Buy Me a
Boat}'' que destaca o que o dinheiro \textit{pode} comprar. Especificamente, o
dinheiro pode comprar um barco, uma caminhonete e um isopor cheio de cerveja! Ele
brinca com a ideia de que o dinheiro é inútil ao destacar que a vida é muito boa
quando você pode comprar alguns brinquedos divertidos.\footnote{\textit{Buy Me a
Boat}, áudio em MP3, faixa 1 em Chris Janson, \textit{Buy Me a Boat}, Warner
Bros., 2015.}

A maioria de nós provavelmente já deve ter aprendido que tanto os Beatles quanto
Chris Janson estão corretos: o dinheiro não é a fonte definitiva de felicidade,
mas ajuda a colocar comida na mesa e um teto sobre as cabeças de nossos
familiares. Infelizmente, as despesas não param por aí. Roupas, serviços públicos,
cuidados infantis, telefones celulares, televisão, internet, viagens, presentes de
Natal, empréstimos estudantis, seguros, cartões de crédito. A lista continua. E
ainda há despesas inesperadas. Não tenho certeza se existe uma compra menos
satisfatória do que um novo aquecedor de água quando o antigo de repente quebra!

Equilibrar as contas é difícil. O Federal Reserve constatou que 46\% dos
americanos não seriam capazes de cobrir uma despesa de emergência de US\$ 400,
como o aquecedor de água novo, com economias em dinheiro, em vez disso, teriam que
pagar ao longo do tempo ou pegar empréstimo.\footnote{Ylan Q. Mui, \textit{The
Shocking Number of Americans Who Can't Cover a \$400 Expense}, Washington Post,
25 de maio de 2016,
https://www.washingtonpost.com/news/wonk/wp/2016/05/25/the-shocking-number-of-americans-who-cant-cover-a-400-expense/?utm\_term=.5df198ec65f4.}
Parece que nossa ansiedade financeira pode ser justificada. Para muitas famílias,
as contas simplesmente não fecham.

Felizmente, servimos a um Deus que, como disse o Pastor Rick Warren, ``se importa
com nossas contas.''\footnote{Rick Warren, em \textit{Trust: God Cares About Your
Bills}, PastorRick.com, acessado em 15 de janeiro de 2018,
http://pastorrick.com/devotional/english/trust-god-cares-about-your-bills.}
Prover para nossas famílias raramente é fácil, mas apesar desses desafios, Deus
nos chama a encontrar paz Nele em vez de sucumbir à ansiedade em relação ao
dinheiro. Jesus disse: ``Não se preocupem com sua própria vida, quanto ao que
comer; nem com seu próprio corpo, quanto ao que vestir. A vida é mais importante
do que a comida, e o corpo, mais do que as roupas.''\footnote{Lc 12:22-23.}

Sempre que eu (Greg) penso nesse versículo, penso no pastor da igreja que
frequentei durante a pós-graduação. Ele e sua esposa adotaram onze filhos e vivem
com o salário de classe média em uma região cara do país. Adotar onze crianças
nunca foi o plano A deles, mas era o plano de Deus. Nem sempre está claro como
eles vão dar conta das despesas, mas de alguma forma as coisas sempre se resolvem
-- frequentemente através de atos incríveis de generosidade dos membros da igreja.
O pastor diz: ``Assim como o apóstolo Paulo, aprendemos a nos contentar tanto com
pouco quanto com muito. Aprendemos a confiar que Deus \textit{vai} prover, mesmo
que nem sempre saibamos exatamente \textit{como} Ele fará isso.''\footnote{Fazendo
referência a Fp 4:12.}

Rick Warren oferece esse desafio: ``Você confia o suficiente em Deus para não se
preocupar com suas circunstâncias? A preocupação é apenas uma forma de
[idolatria], porque toda vez que você se preocupa, está dizendo: `Tudo depende de
mim.' Você deve confiar sua vida a Deus. Enquanto você amar qualquer coisa mais do
que a Deus, essa coisa, pessoa ou item se tornará uma fonte de
ansiedade.''\footnote{Warren, em \textit{Trust: God Cares About Your Bills}.}

Para deixar claro, não podemos viver da maneira que quisermos, gastar todo o nosso
dinheiro em coisas frívolas, acabar sem dinheiro e simplesmente dizer: ``Deus
proverá.'' Deus deseja que utilizemos \textit{de forma responsável} os recursos
que Ele nos dá para a nossa provisão. Além disso, a dura verdade é que algumas
pessoas não têm tudo o que precisam, muitas vezes sem culpa própria. Por exemplo,
refugiados que fogem de um país devastado pela guerra ou uma criança cujos pais
são viciados em drogas.

Essas são questões difíceis, muitas vezes sem respostas claras. A pobreza e o
sofrimento são parte de nossa realidade neste mundo quebrado e pecaminoso, e esses
desafios talvez nunca sejam completamente resolvidos até o retorno de Cristo.
Enquanto isso, o chamado claro de Cristo é confiar nele, independentemente de
nossas circunstâncias, e fixar nossos olhos no que está por vir, mesmo
reconhecendo as lutas legítimas que nós e os outros enfrentamos hoje.
