\section*{O que Jesus disse}

Se quisermos viver as melhores, mais felizes e mais significativas vidas
possíveis, precisamos buscar os ensinamentos de Jesus na Bíblia. O dinheiro foi um
tema central em muitos dos seus ensinamentos e parábolas. Isso é consistente com a
Bíblia como um todo, que contém cerca de 2.350 versículos relacionados a dinheiro,
posses e nossa atitude em relação a eles. Ao contrário da maioria do que ouvimos
sobre dinheiro hoje em dia, o plano de Jesus para uma vida financeira abundante
não era garantir que todos se tornassem ricos. Na verdade, mesmo que a riqueza
financeira e a estabilidade pareçam coisas boas, não temos registro de Jesus nos
encorajando a persegui-las. Nem uma vez sequer!\footnote{Algumas pessoas citam
Lucas 14:28 para sustentar a ideia de economizar dinheiro, já que Jesus observa
que qualquer pessoa que esteja construindo uma torre deve calcular os custos. Na
verdade, isso é o oposto do que Jesus quis dizer. A citação em questão faz parte
da explicação de Jesus sobre o custo do discipulado. A metáfora da torre ilustra o
sacrifício total que os discípulos devem fazer, e Jesus continua no mesmo
parágrafo dizendo: ``Assim, qualquer um de vocês que não renunciar a tudo o que
possui não pode ser meu discípulo.'' Isso está longe de ser um incentivo para
economizar dinheiro!} Em vez disso, ele ensinava sobre dinheiro para inspirar as
pessoas a terem um relacionamento mais próximo com Deus, a quem ele chamava de
``nosso Pai''. Para ele, finanças pessoais não se tratavam de seguir regras
financeiras; tratava-se de um relacionamento dinâmico de confiança com Deus, uma
jornada em direção às riquezas que são, em última instância, as mais gratificantes.

Nossa forma de lidar com o dinheiro pode nos levar às verdadeiras riquezas de um
relacionamento mais profundo com Jesus, marcado pela \emph{gratidão},
\emph{contentamento}, \emph{confiança} e \emph{amor}. Mas se buscarmos o dinheiro
por si só, estaremos perseguindo riquezas falsas, e nossas vidas serão marcadas
por \emph{orgulho}, \emph{cobiça}, \emph{ansiedade} e \emph{indiferença},
promovendo uma trágica separação de Deus e da alegria que ele oferece. Assim, os
ensinamentos de Jesus focam menos na obtenção de riquezas e mais em como nosso
relacionamento com o dinheiro molda nosso caráter.

Curiosamente, muitos dos personagens financeiramente estáveis e diligentes que
encontramos na Bíblia foram condenados especificamente por suas atitudes em
relação ao dinheiro. O rei Salum, Ananias e Safira, o rico insensato, o jovem
rico, o rico da história de Lázaro e toda a igreja em Laodiceia vêm à mente como
alguns exemplos. Qualquer um deles poderia ter sido um caso de sucesso encontrado
na revista \emph{Money}, mas aos olhos de Deus, eles falharam. Eles buscaram o
dinheiro por si só, negligenciando suas oportunidades de servir a Deus e aos
outros financeiramente, e isso resultou em juízo. Sim, existem alguns líderes
ricos que são elogiados (como Lídia, Cornélio e Zaqueu), mas eles parecem ser a
exceção em vez da regra.

Como ilustrado nas Escrituras, o dinheiro é uma força enganadora que pode capturar
os corações das pessoas, afastando-as de Deus como um poderoso ímã. Quanto mais
perto você chega de um ímã, mais forte ele puxa -- e quanto mais você entrega seu
coração ao dinheiro, mais enredado você se tornará. De fato, Jesus identificou o
dinheiro como o principal concorrente pelas afeições do coração humano quando
disse: ``Ninguém pode servir a dois senhores... Vocês não podem servir a Deus e ao
Dinheiro''\footnote{Mt 6:24.}.

Seria sábio da nossa parte prestar atenção ao aviso proveniente das vidas dessas
pessoas na Bíblia. Mesmo que sejamos responsáveis e diligentes, tenhamos
orçamentos perfeitamente organizados em nossos \emph{planners} e estejamos
economizando regularmente para a aposentadoria, ainda podemos fracassar
espiritualmente quando se trata de administrar o que nos foi dado.
