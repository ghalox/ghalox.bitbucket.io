\section*{A Lista de Tarefas de Deus}

Ao longo das Escrituras, Deus revela três prioridades principais -- três grandes
coisas nas quais Ele está envolvido no mundo. Ele nos convida a ajudar em cada uma
delas, e temos a oportunidade de nos unir ao nosso Pai eterno em sua obra.
Conforme assumimos essas tarefas, Deus aumenta nossa capacidade de amar,
libertando-nos da indiferença.\footnote{Não tem uma razão em específico para a
ordem em que vamos apresentar essas tarefas. Todas as três são mandatos claros da
Escritura.}

\subsection*{Tarefa 1: Servir aos Pobres (Misericórdia e Justiça)}

Ao longo dos séculos, os cristãos têm sido movidos pelo coração de Deus pelos
pobres. Lembre-se do pai da igreja primitiva Basílio de Cesareia? Há mais de
dezesseis séculos, ele pregou um sermão sobre esse assunto:

\begin{quotation}
Não posso ser rico enquanto eles passam necessidade, nem desfrutar de boa saúde
sem tentar curar suas feridas, nem ter comida abundante, roupas boas e um teto sob
o qual possa descansar, a menos que lhes ofereça um pedaço de pão e lhes dê,
conforme posso, parte das minhas roupas e abrigo sob o meu teto.
\end{quotation}

Essas não eram palavras vazias. Basílio cresceu rico, mas vendeu a maioria de sua
herança para servir aos pobres em sua região. Muitos outros cristãos fizeram
coisas semelhantes ao longo dos tempos. Por que os cristãos se importam tanto com
os pobres? Porque Deus se importa.

Por exemplo, o Salmo 113 descreve a glória e a grandiosidade de Deus em sua
majestade celestial, mas então muda de direção, afirmando que Ele ``olha para a
terra do alto'' e ``levanta o pobre do pó e ergue o necessitado do monte de
cinzas''. O Salmo 68 identifica Deus como o ``Pai dos órfãos e protetor das
viúvas''. O Salmo 82 vai além. Em um concurso poético entre os deuses, o nosso
Deus é o maior deus não por causa de seu poder onipotente, mas sim porque Ele
concede ``justiça aos fracos e órfãos'' e ``resgata os fracos e necessitados''.

Jesus instruiu seus discípulos a amarem o seu próximo como a si mesmos. Não
costumamos parar para pensar nas implicações disso, mas o conceito ganhou vida
para nós (John e Megan) quando descobrimos que nossa criança apadrinhada em
Honduras tinha um problema nos olhos. Sentimos uma conexão pessoal com essa
menininha de quatro anos e sabíamos que ela não tinha recursos para receber o
tratamento médico adequado. Nós gastaríamos 1.000 dólares com os olhos de nosso
próprio filho? Sim, sem hesitação. E, pela primeira vez, sentimos o amor de Deus
se manifestar tão intensamente dentro de nós por uma criança que não era nossa.
Escrevemos rapidamente para o ministério, pedindo ajuda para determinar se nossa
assistência financeira poderia resolver esse problema médico. Pagaríamos qualquer
custo para ajudá-la.

Jesus afirmou a importância de servir aos pobres, inclusive contando uma parábola
sobre como os copos de água fria que damos ou negamos aos pobres são
contabilizados por Deus.\footnote{A parábola das ovelhas e dos bodes: Mt
25:31-46.} Em sua história, aqueles que deixam de servir aos pobres são lançados
no inferno! A Bíblia ensina que a salvação é pela graça somente, através da fé
somente. Não podemos conquistar nosso caminho para o céu fazendo boas ações.
Então, por que Jesus contou essa história?

Talvez Jesus esteja nos dizendo que nunca realmente o conhecemos como Salvador e
Senhor, a menos que o conheçamos o suficiente para perceber que o negócio dele é
cuidar dos oprimidos. Como escreveu Tiago, irmão de Jesus, a religião pura se
encontra em cuidar dos órfãos e viúvas em sua necessidade.\footnote{Tg 1:27.}

Reivindicar ser cristão, mas não dar aos pobres, é como reivindicar ser um chef,
mas não saber cozinhar. Isso simplesmente não faz sentido; é uma contradição de
termos. Cristãos cuidam dos necessitados; está em nosso DNA espiritual. Como disse
o pai da igreja João Crisóstomo (c. 347–407 d.C.), ``Não me diga que você não pode
cuidar dos outros. Se vocês são cristãos, o impossível é não cuidarem
deles''.\footnote{João Crisóstomo, em \textit{On Wealth and Poverty}. Tradução
própria.}

\subsection*{Tarefa 2: Salvar os Perdidos (Evangelismo)}

Mais de uma vez, a instrução de Jesus depois de ajudar ou curar alguém foi: ``Vá e
não peque mais.'' Que coisa estranha de se dizer! Por que não ``Aproveite a sua
vida! Seja abençoado!'' Uma vez, antes de curar um homem paralítico, ele disse:
``Filho, os seus pecados estão perdoados.'' Por que ele diria isso?

Jesus se importava com a pessoa como um todo. Ele alimentava os famintos e curava
os doentes, mas o fazia no contexto de uma cura holística, incluindo um convite
para entrar no reino de Deus. Ele sabia que havia um valor limitado em deixar as
pessoas mais confortáveis na vida se elas ainda estivessem se encaminhando para
uma separação eterna de Deus. Ele queria salvá-las verdadeiramente, para sempre.
Quando ele olhava para uma pessoa paralisada, seu primeiro pensamento era:
\textit{Eles precisam de perdão pelos seus pecados.} A dor física e o sofrimento
são secundários em relação à nossa realidade espiritual.

De fato, a última instrução de Jesus para seus discípulos, conhecida como a
Grande Comissão, foi a seguinte:

\begin{quotation}
Portanto, vão e façam discípulos de todas as nações, batizando-os em nome do Pai e
do Filho e do Espírito Santo, ensinando-os a obedecer a tudo o que eu lhes
ordenei.\footnote{Mt 28:18-20.}
\end{quotation}

Jesus tinha uma visão clara de que seu evangelho alcançaria os confins da Terra,
dando a todas as pessoas a chance de responder ao convite de Deus. Hoje, dois mil
anos após Jesus ter caminhado na terra, estamos próximos de alcançar esse
objetivo. Talvez você não seja chamado especificamente a ir para os cantos mais
distantes do mundo para compartilhar o evangelho, mas você pode dar generosamente
para apoiar aqueles que o fazem.

A tradução da Bíblia proporciona uma perspectiva única sobre o evangelismo global.
A Bíblia foi traduzida com sucesso para milhares de idiomas, mas ainda existem
aproximadamente 1.900 idiomas falados na Terra que não possuem nenhuma
Bíblia.\footnote{\textit{Every Tribe Every Nation: Eradicating Biblical Poverty},
acessado em 9 de agosto de 2018. https://www.everytribeeverynation.org.}
Milhares de pessoas estão trabalhando diligentemente para continuar a tradução, e
centenas de milhares de fiéis generosos estão financiando esse trabalho. Muitos
acreditam que o último tradutor da Bíblia já nasceu e que vamos concluir essa
tarefa em nossa vida.\footnote{O esforço pioneiro \textit{IllumiNations} foi
empreendido por dez das maiores agências de tradução da Bíblia -- um esforço para
compartilhar dados, colaborar na tarefa compartilhada de acabar com a pobreza da
Bíblia e alocar recursos em diferentes ministérios de acordo com sua participação
na missão maior. Você pode saber mais sobre esse esforço em
https://www.illuminations.bible.}

Milhões de pessoas ainda esperam que alguém lhes traga libertação de sua maior
pobreza de todas: A separação de Deus. Elas precisam da Bíblia. Elas precisam de
igrejas e cuidado pastoral. Elas precisam de uma comunhão de crentes.

Em nossa cultura pós-moderna, não é popular afirmar declarações de verdade ou
imaginar que alguém precise ter seu sistema de crenças mudado. No entanto, dois
mais dois ainda é igual a quatro, e Jesus ainda é o único caminho para Deus. Negar
isso é negar o cerne do cristianismo, e se realmente acreditamos em Jesus, devemos
acreditar que sua mensagem de esperança é uma mensagem para toda a humanidade. A
reconciliação com Deus, por meio de Jesus, é a coisa mais importante que alguém
pode experimentar.

Apesar da clareza e força da Grande Comissão, menos de 0,1\% da renda cristã é
destinada às missões estrangeiras globais (menos de oito dólares por pessoa, por
ano).\footnote{David B. Barrett e Todd M. Johnson, \textit{World Christian Trends,
AD 30–AD 2200: Interpreting the Annual Christian Megacensus} (Pasadena, CA:
William Carey Library, 2001), 655.} O amor mais verdadeiro que podemos mostrar aos
nossos próximos ao redor do mundo é oferecer a eles a oportunidade de conhecer o
Deus que os criou e os ama.

\subsection*{Tarefa 3: Fortalecer os Crentes (Discipulado)}

A primeira metade da Grande Comissão é o batismo, ou salvação. A segunda metade é
``ensinando-os a observar tudo o que eu ordenei''. Essa é a principal e contínua
tarefa das nossas igrejas locais. Jesus se preocupa profundamente com seus
seguidores tendo uma fé amadurecida, em crescimento e saudável, que orienta suas
vidas diárias. De fato, Jesus instrui claramente que aqueles que fornecem
instrução espiritual devem ser bem remunerados: ``Os presbíteros que lideram bem a
igreja são dignos de dupla honra, especialmente aqueles cujo trabalho é a
pregação e o ensino, pois a Escritura diz: `Não amordace o boi enquanto está
debulhando o cereal', e `o trabalhador merece o seu salário.'''\footnote{1 Tm
5:17-18.}

\begin{ideabox}
Considere escrever sobre sua experiência de doação favorita a cada ano. Com o
tempo, você poderá olhar para trás e ver como Deus o abençoou e abençoou os outros
por meio da sua generosidade! Você pode até compartilhar essas histórias com seus
filhos ou outros amigos próximos como testemunho da bondade de Deus.
\end{ideabox}

Muitos membros da equipe da igreja assumem seus empregos como resposta ao chamado
de Deus em suas vidas, abrindo mão de uma remuneração significativamente maior
para servir ao povo de Deus. Em uma igreja grande, o pastor sênior e o pastor
executivo são o CEO e o COO de uma organização multimilionária. Eles contratam e
demitem funcionários, estabelecem estratégias, estabelecem visão, cultivam
cultura, gerenciam relacionamentos com o conselho e enfrentam desafios toda
semana. Eu mencionei que eles precisam ter um excelente sermão preparado a cada
fim de semana? E eles fazem tudo isso enquanto potencialmente ganham metade do que
poderiam ganhar no setor privado!

A igreja é o plano de Deus para o mundo -- seu veículo escolhido para expandir sua
família e servir aos pobres e salvar os perdidos. Sem nossas igrejas locais, não
teríamos um lugar para levar nossos filhos para a adoração regular, nenhuma fonte
constante de ensino bíblico para nos submetermos, nenhum lugar de encontro para os
crentes. Deus está totalmente comprometido com a igreja. E você?

Devemos fazer as mesmas perguntas às nossas igrejas que faríamos a qualquer
organização sem fins lucrativos em relação a como elas usam diligentemente seus
recursos. Mas também devemos saber que temos uma grande vantagem como potenciais
apoiadores de nossas igrejas, em relação a outras organizações sem fins
lucrativos. Já estamos fazendo a devida diligência -- visitamos todas as semanas,
conhecemos a equipe e testemunhamos grande parte do trabalho em primeira mão. A
maioria de nós sabe muito menos sobre as instituições de caridade que apoiamos do
que sabemos sobre nossas igrejas locais.

Aqui estão duas perguntas básicas a serem feitas ao considerar um apoio generoso à
sua congregação local:

\begin{itemize}
\item \textit{Minha igreja está em missão?} Minha igreja fortalece os crentes,
capacitando-os a amadurecer em Cristo? Além disso, minha igreja tem iniciativas
voltadas para o exterior, para servir aos pobres e salvar os perdidos? (Muitas
igrejas que admiramos destinam de 10\% a 15\% ou mais do seu orçamento para
projetos externos, para obras missionárias ou para servir aos pobres globalmente.)

\item \textit{A liderança da minha igreja é responsável perante outros, financeira
e pessoalmente?} Há um conselho de presbíteros que supervisiona a liderança? A
quem os líderes pastorais são responsáveis? Algumas igrejas grandes são membros do
Conselho Evangélico de Prestação de Contas Financeiras (ECFA, na sigla em inglês).
Outras igrejas publicam seus orçamentos para que os membros possam ver. Seja qual
for o método, uma igreja deve garantir que sua liderança e orçamento estejam
sujeitos a escrutínio externo. (Observação: isso não significa necessariamente que
você tem o direito de ver o orçamento. Apenas significa que deve haver um
conselho, uma agência de auditoria ou algum mecanismo equivalente para garantir a
responsabilidade.)
\end{itemize}

Se sua igreja está em missão para Deus e tem uma liderança responsável, você pode
dar com confiança. Se não estiver, você pode considerar em oração a defesa por
mudanças ou encontrar um novo lar espiritual.

\subsection*{E Quanto a ``Outras'' Formas de Doação?}

Discutimos as três tarefas claras de doação nas Escrituras, mas também enfrentamos
dilemas sobre se devemos doar para outras causas. Ambos temos sido modestos
apoiadores de nossas instituições de ensino, apreciamos as artes e os museus, e
não somos contrários a pesquisas médicas ou muitas outras grandes causas.

Ao orarmos sobre essa questão, fomos convencidos de que, como cristãos, nosso foco
principal na doação deve estar nas tarefas que Deus claramente nos deu, em sua
infinita sabedoria e bondade. Portanto, enquanto podemos enviar cinquenta dólares
para nossa alma mater como um gesto de gratidão, reservaremos a grande maioria de
nossas doações para as três principais tarefas: Servir aos pobres, salvar os
perdidos e fortalecer os crentes.

Na verdade, Santo Agostinho, um gigante da fé cristã primitiva e um dos teólogos
mais respeitados de todos os tempos, abordou exatamente essa questão há dezessete
séculos. Cidadãos romanos ricos estavam se juntando à igreja, e o ``amor pela
cidade'' estava em conflito com o ``amor pelos pobres''. Esses filantropos
experientes estavam acostumados a doar para projetos de arte, atividades cívicas e
construções públicas, mas Agostinho os desafiou a adotar a missão cristã de servir
aos pobres em nome de Jesus.
