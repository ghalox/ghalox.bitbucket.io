\section*{Amando Menos do que Pensamos}

Tendemos a pensar muito bem de nós mesmos. Na verdade, 93\% dos americanos se
consideram motoristas acima da média.\footnote{Ola Svenson, \textit{Are We All
Less Risky and More Skillful Than Our Fellow Drivers?} Acta Psychologica 47, no. 2
(Fevereiro de 1981): 143–48,
http://heatherlench.com/wp-content/uploads/2008/07/svenson.pdf,
doi:10.1016/0001–6918(81)90005–6.} Devido a um viés cognitivo conhecido como
superioridade ilusória, achamos que somos melhores do que os outros, mesmo quando
não somos. Isso é verdade não apenas para habilidades de direção, mas também para
generosidade. \textit{Todo mundo} se considera ``uma pessoa generosa''. Mas será
que somos mesmo?

O gráfico a seguir mostra as respostas de cristãos da geração millenial em uma
grande pesquisa.\footnote{\textit{The Generosity Gap}, Barna Group e Thrivent
Financial, https://www.barna.com/generosity/.} Setenta por cento afirmam ser
generosos com dinheiro, mas apenas 16\% doaram pelo menos cinquenta dólares para
uma igreja ou instituição de caridade no ano anterior. É difícil imaginar como
alguém pode ser classificado como ``generoso'' se doou menos de cinquenta dólares
durante todo um ano. (A propósito, não coloquem a culpa apenas em nós,
millennials; as estatísticas para outras gerações foram semelhantes.)

\begin{center}
\textbf{Nós Somos Menos Generosos do que Pensamos} \medskip

\begin{figure}[h]
\centering
\begin{tikzpicture}
\node [left] at (0, 0) {0\%};
\node [left] at (0, 0.7) {10\%};
\node [left] at (0, 1.4) {20\%};
\node [left] at (0, 2.1) {30\%};
\node [left] at (0, 2.8) {40\%};
\node [left] at (0, 3.5) {50\%};
\node [left] at (0, 4.2) {60\%};
\node [left] at (0, 4.9) {70\%};
\node [left] at (0, 5.6) {80\%};

\draw (0, 0) -- (6, 0);
\draw (0, 0.7) -- (6, 0.7);
\draw (0, 1.4) -- (6, 1.4);
\draw (0, 2.1) -- (6, 2.1);
\draw (0, 2.8) -- (6, 2.8);
\draw (0, 3.5) -- (6, 3.5);
\draw (0, 4.2) -- (6, 4.2);
\draw (0, 4.9) -- (6, 4.9);
\draw (0, 5.6) -- (6, 5.6);

\fill [lightgray] (1, 0) rectangle (2, 4.9);
\fill [lightgray] (4, 0) rectangle (5, 1.1);

\node [below] at (1.5, 0) {\small Eu sou generoso};
\node [below] at (1.5, -0.3) {\small com dinheiro};
\node [below] at (4.5, 0) {\small Eu doei mais que};
\node [below] at (4.5, -0.3) {\small US\$ 50 ano passado.};
\end{tikzpicture}

\caption{A coluna da esquerda inclui aqueles que se declararam como ``muito'' ou
``um tanto'' generosos com dinheiro. Ainda mais chocantemente, apenas 2\% dos
cristãos millenials deram mais que US\$ 2.500 ano passado a igrejas e instituições
de caridade.}
\end{figure}
\end{center}

Jesus contou uma história com a intenção de nos fazer refletir sobre esse viés
cognitivo e nos motivar a agir em relação à nossa generosidade (Lucas 16:19-31).
Ele explicou que havia um homem rico com roupas esplêndidas e comida fina em sua
bela casa. Do lado de fora estava um mendigo sem-teto, chamado Lázaro, coberto de
chagas, faminto e negligenciado. O homem rico nunca prejudicou ativamente Lázaro,
mas também nunca tentou ajudá-lo. Ambos acabaram morrendo. Lázaro foi para o céu,
enquanto o homem rico encontrou-se em tormento eterno. Ele pediu alívio e foi
lembrado de que ele teve sua parte de conforto durante sua vida na terra. Ele
pediu para que seus irmãos vivos fossem avisados, mas foi-lhe dito que se eles não
tivessem ouvido o mandamento das Escrituras para amar os necessitados, eles não
ouviriam uma mensagem vinda do além. A mensagem básica para o homem rico foi:
``Você sabia o que fazer e perdeu sua chance!''

Observe que Jesus não perguntou se o homem rico se sentia ``chamado'' a ajudar os
pobres. Quase podemos ouvir o homem rico dizendo a um amigo: ``Sabe, eu nunca me
senti inclinado a ajudar os pobres. Eu sei que algumas pessoas realmente se
importam com a pobreza, mas isso simplesmente nunca foi minha praia.'' Com
certeza, ele se considerava uma pessoa generosa, e suas festas de jantar
provavelmente eram fantásticas (é fácil ser generoso com nossos amigos próximos).
Apesar disso, ele ainda falhou em ouvir a instrução de Deus para servir os pobres.

A pergunta séria que Jesus quer que façamos depois de ouvir essa história é: ``Eu
sou o homem rico?''

Como experimento mental, vamos imaginar que a população mundial tenha sido
reduzida a apenas dez famílias que vivem juntas em uma única rua, em dez casas
sucessivas, das mais pobres às mais ricas.\footnote{O crédito para essa ideia vai
para \textit{Dollar Street} (https://www.dollarstreet.org/). Eles têm uma grande
coleção de fotos da vida real de famílias ao redor do mundo, com todo o tipo de
renda.}

Em uma extremidade da rua, há uma família que não tem casa alguma e está em
situação de escravidão ou enfrentando a fome. A segunda família, seu vizinho, vive
em um casebre vazando com chão de terra, come mingau básico todos os dias e luta
pela sobrevivência, indo descalços até o riacho buscar água suja para beber e
tomar banho. Essas duas famílias são os ``Lázaros''.

As seis casas do meio, representando a maioria das famílias no mundo hoje, têm
casas de cimento básicas de qualidade crescente à medida que avançamos pela rua.
Seus telhados geralmente não têm vazamentos, embora um vento frio passe pelas
paredes no inverno, e a maioria não pode pagar ar-condicionado, então o verão pode
ser sufocante. Essas casas têm água encanada, bicicletas ou motocicletas para
transporte, e eletricidade que funciona na maior parte do tempo. Essas famílias
possuem conveniências como escovas de dente, geladeiras e sapatos. Suas vidas são
relativamente estáveis e saudáveis, embora trabalhem incrivelmente duro e não
consigam imaginar ter um carro ou tirar férias tranquilas.

Finalmente, na parte rica da rua, há duas famílias que surpreendem seus vizinhos
ao ganharem dezenas de milhares de dólares por ano. Suas casas têm vários quartos,
sistemas avançados de aquecimento e refrigeração e cozinhas completas. Enquanto a
família mais pobre vai descalça buscar água, a casa mais rica tem uma piscina
cheia de água limpa apenas para fins recreativos! Essas famílias ricas têm
garagens com carros estacionados, se comunicam por smartphones e fazem uma ou duas
viagens básicas de férias por ano. Elas podem não ter milhões de dólares, mas
nunca deixam de fazer uma refeição por causa da fome ou precisam ir ao trabalho ou
à escola descalças.

Esta tabela mostra uma imagem estatisticamente precisa do que cada família
ganharia (ajustado para diferenças de custo de vida entre regiões
geográficas).\footnote{Global Rich List, http://www.globalrichlist.com; Christoph
Lakner and Branko Milanovic, \textit{Global Income Distribution: From the Fall of
the Berlin Wall to the Great Recession,} The World Bank, dezembro de 2013,
http://documents.worldbank.org/curated/en/914431468162277879/pdf/WPS6719.pdf;
Rakesh Kochhar, \textit{A Global Middle Class Is More Promise Than Reality}, Pew
Research Center, 8 de julho de 2015,
http://www.pewglobal.org/2015/07/08/a-global-middle-class-is-more-promise-than-reality/\#who-is-middle-income.}

\begin{table}
\centering
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|}
\hline
Família & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
\hline
Renda anual & \$1k & \$2k & \$2.5k & \$3k & \$4k & \$6k & \$8k & \$12k & \$30k &
\$54k \\
\hline
\end{tabular}
\caption{Os dados foram ajustados por Paridade do Poder de Compra para refletir os
diferentes custos de vida ao redor do mundo. Essa tabela é baseada assumindo
famílias de três pessoas. Se sua família for menor ou maior, os números podem
estar um tanto desajustados, mas assim são ilustrativos.}
\end{table}

Para cada dólar que a família \#1 gasta, a família \#10 tem mais de cinquenta
dólares para usar! Qual casa é mais representativa daquela em que você
estaria morando, se esta fosse a sua rua? Vá em frente e circule a casa que mais
se assemelha à sua. Se você está lendo este livro, vamos supor que seja a mais
rica ou a segunda mais rica. Talvez sua família ganhe muito mais de 54.000 dólares
por ano, o que significa que seus ganhos ultrapassam os da família mais rica! Se
sua família ganha a renda mediana nos Estados Unidos ou mais, você pode se
surpreender ao saber que, entre dez famílias escolhidas aleatoriamente ao redor do
mundo, você provavelmente seria o mais rico. Mesmo aqueles que podem se sentir
pobres nos Estados Unidos -- aqueles que ganham 30.000 dólares por ano -- estão
entre as pessoas mais abençoadas financeiramente do mundo.\footnote{A realidade do
Brasil é bem diferente. Considerando dados do Critério Brasil 2021/PNAD Contínua
2020, a renda média para as classes D e E era de R\$ 862 mensais. Juntas, elas
compõem 28,8\% da população. Para efeitos de comparação, as classes C1 (20,4\%) e
C2 (27,2\%) tinham renda média de R\$ 3.194,33 e R\$ 1.894,95. As classes A
(2,8\%), B1 (4,6\%) e B2 (16,2\%) tinham renda média de R\$ 22.749,24, R\$
10.788,56 e R\$ 5.721,72. \textit{Critério de Classificação Econômica do Brasil
2021}, Associação Brasileira de Empresas de Pesquisa.
https://www.abep.org/criterioBr/01\_cceb\_2021.pdf. (Nota do Editor.)}

O objetivo aqui não é sentir culpa se você é financeiramente abençoado. Na
verdade, Paulo escreve que Deus ``tudo nos provê ricamente, para a nossa
satisfação''.\footnote{1 Tm 6:17.} A família Cortines, assim como a família mais
rica em nossa rua fictícia, possui uma piscina e a recebemos como um presente
gracioso de Deus. No entanto, estamos profundamente conscientes de que é um
presente luxuoso e buscamos cultivar empatia, compaixão e amor pelas famílias do
outro lado da rua -- e esse amor deve se manifestar em atos tangíveis de
generosidade.

Jesus falou com firmeza sobre o homem rico cuja família também estaria no lado
rico da rua. Ele afirmou que aqueles que são abençoados com grandes recursos -- ou
seja, aqueles cujas famílias ganham talvez 30.000 dólares por ano ou mais na
economia moderna -- têm a responsabilidade especial de compartilhar generosamente
com pessoas que enfrentam fome e escassez. Não se trata de justiça, socialismo,
ética utilitária ou honra. Trata-se de amor -- um amor que busca amar aos outros
como amamos a nós mesmos, assim como Jesus nos ensinou.

Lembre-se: Nossos sentimentos não são o fator importante. Nossas ações é que
contam. A grande maioria dos leitores, assim como nós, são o homem rico. Deus nos
convida a amar a Lázaro. Não precisamos necessariamente sentir compaixão o tempo
todo (eu mesmo ainda luto com isso). Apenas precisamos viver e dar em nome dos
outros. Quando nossa paixão por ajudar os necessitados é menor do que o amor de
Deus pelos necessitados, enfrentamos uma lacuna de fé. Tudo o que é necessário
para preencher essa lacuna é dar um passo de fé ousado na generosidade -- o tipo
de passo que Sheila estaria dando antes mesmo de perceber.
