\section*{O Meu Orgulho e o Seu}

Qual é a sua maior conquista na vida? A maioria de nós pode rapidamente listar
alguns candidatos. Talvez pensemos em nossos empregos, nossas famílias ou nossas
posses. Vivemos em uma cultura que celebra as conquistas individuais. O homem
feito é o nosso ícone de sucesso. É verdade que a maioria de nossas maiores
realizações -- formaturas, grandes promoções, a casa própria -- envolvem muito
trabalho árduo de nossa parte. Mas vivemos em um ecossistema complexo de fatores
interconectados; nada se deve exclusivamente aos nossos próprios esforços.

Pense sobre ganhar dinheiro no seu trabalho. Suas habilidades foram dadas por
Deus (Sl 144:1). Seu emprego em si é um presente de Deus (Êx 20:9), e a ordem
política sob a qual sua empresa tem licença para operar é ordenada por Deus (Rm
13:1-2). Até mesmo o planeta em que você está tem a mistura precisa de gases de
que você precisa para respirar e a temperatura ideal para você viver, oferecendo
também uma proteção atmosférica contra raios cósmicos mortais. Todo o universo é
mantido unido a cada momento pela mão graciosa de Deus (Cl 1:17). Sem mencionar
seus pais, professores, treinadores, amigos, pastores e inúmeras outras pessoas
que investiram em você em momentos críticos de sua vida - eles também são um
presente de Deus!

Se removermos qualquer um desses fatores, nossas conquistas simplesmente
desapareceriam. Refletir sobre a fragilidade de nossos sucessos - quão
contingentes eles são - nos liberta para abrir mão do orgulho e agradecer ao Deus
que os tornou possíveis. Ele é o fator mais central de tudo. Nossa economia
atribui riqueza e renda aos indivíduos, mas se nos deixarmos levar pensando que o
dinheiro é gerado por nossos próprios esforços isolados, nos enganamos na
armadilha do orgulho. É muito melhor reconhecer cada centavo como um presente do
nosso gracioso Pai - um presente precioso a ser valorizado, administrado e
compartilhado.

A ideia de que não podemos nos sustentar por conta própria está no cerne da fé
cristã. Sozinhos, você e eu não conseguiríamos cumprir a missão espiritual, ética
ou social. A maioria de nós já ouviu o versículo: ``Todos pecaram e estão
destituídos da glória de Deus''.\footnote{Rm 3:23.} É verdade! Somente em Jesus
somos completos, e ele é a fonte do ar em nossos pulmões, do dinheiro em nossas
contas bancárias e das verdadeiras riquezas da vida em relacionamento com Deus --
agora e pela eternidade.

Mas nosso senso de auto-suficiência, nosso orgulho, é difícil de admitir, e muitas
vezes se infiltra lentamente e sorrateiramente, como podemos ver na história de
Davi.

Davi foi o maior e mais famoso rei do antigo Israel. Sua vida foi incrivelmente
complexa e cheia de intriga, drama e aventura -- material digno de um enredo de
filme de Hollywood. Ele decapitou um gigante, escapou por pouco de lanças lançadas
em sua direção e uma vez dançou quase nu pelas ruas em celebração jubilante. Ele
também era um intelectual, tornando-se o autor mais vendido de poesia na história
mundial.\footnote{Ele escreveu aproximadamente metade dos salmos da Bíblia.} Ele
enfrentou muitos momentos de desespero, mas manteve uma fé inabalável de que Deus
era tanto o rei soberano do mundo quanto o autor de sua própria história de vida.
Que cara!

Infelizmente, seus erros foram tão épicos quanto seus sucessos. Ele não apenas
dormiu com a esposa do seu próximo, mas também o matou para se livrar dele. Ele
massacrou exércitos inteiros, e seu histórico de guerra sangrenta foi o motivo
pelo qual Deus o impediu de construir o templo em Jerusalém. E, por fim, ele
também caiu vítima do orgulho. Vamos dar uma olhada nessa história do livro de
Samuel:

\begin{quotation}
Satanás levantou-se contra Israel e levou Davi a fazer um recenseamento do povo.
Davi disse a Joabe e aos outros comandantes do exército: ``Vão e contem os
israelitas desde Berseba até Dã e tragam-me um relatório para que eu saiba quantos
são''. Joabe, porém, respondeu: ``Que o Senhor multiplique o povo dele por cem. Ó
rei, meu senhor, não são, porventura, todos eles súditos do meu senhor? Por que o
meu senhor deseja fazer isso? Por que deveria trazer culpa sobre Israel?'' Mas a
palavra do rei prevaleceu, de modo que Joabe partiu, percorreu todo o Israel e
então voltou a Jerusalém.\footnote{1 Cr 21:1-4.}
\end{quotation}

À primeira vista, essa história parece inocente o suficiente. Afinal, governos ao
redor do mundo realizam censos regularmente. Israel deveria viver como o povo
escolhido de Deus, com seu rei humildemente dependente de Deus. No mundo antigo,
havia apenas duas razões para um censo: impor novos impostos ou se preparar para
campanhas militares. O desejo de Davi por um censo significava que ele estava
planejando aumentar seu poder real, sonhando com aumento de receita ou um exército
mais forte, coisas que ele mesmo poderia colocar em movimento, mesmo que Deus não
o tivesse chamado para fazê-lo. Isso implicava uma falta de fé em Deus como
provedor, com Davi tomando conta do destino da nação e buscando mais do que já lhe
havia sido dado. A história continua dizendo que ``Deus ficou descontente com essa
coisa'' e graves consequências se seguiram.

Talvez não possamos competir com a vida do Rei Davi em termos de aventura,
derramamento de sangue ou intriga, mas compartilhamos sua luta contra o orgulho e
a autoconfiança. Quantas vezes imaginamos quanto dinheiro faremos no próximo ano,
ou calculamos que tipo de casa poderemos comprar um dia? E o que dizer de olhar
para baixo, com um pouco de superioridade, para aqueles com menos dinheiro ou
status do que nós? Todos nós provavelmente fomos culpados disso ao longo de nossas
vidas. Esses pequenos atos são nossa versão do censo do Rei Davi. Assim como Deus
queria que Israel dependesse humildemente dele, ele deseja o mesmo de nós.

Uma carta na Bíblia foi escrita para abordar exatamente esse ponto, cerca de mil
anos após a vida de Davi ter terminado. Foi escrita para a comunidade dos
seguidores de Jesus em Laodiceia, mas poderia muito bem ter sido escrita para nós
hoje.

Laodiceia, assim como muitas nações hoje em dia, era próspera e orgulhosa disso.
Muitos cidadãos tinham encanamento, havia um teatro na cidade e o mercado estava
repleto de atividades. Eles tinham tecnologia médica de ponta, e você podia
comprar mercadorias da Ásia, África ou Itália, tudo sem sair da
cidade.\footnote{Jeff Manion, \textit{Satisfied} (Grand Rapids: Zondervan, 2013),
cap. 18.} Eles se orgulhavam de quão bem-sucedidos eram, até recusando assistência
do governo romano ao se recuperarem de um terrível terremoto, adotando um espírito
de "nós conseguimos fazer por nós mesmos".

Mas Jesus tinha palavras duras para a igreja abastada em Laodiceia:

\begin{quotation}
Conheço as suas obras, sei que você não é frio nem quente. Melhor seria que você
fosse frio ou quente! Assim, porque você é morno, não é frio nem quente, estou
a ponto de vomitá-lo da minha boca. Você diz: \textit{`Estou rico, adquiri
riquezas e não preciso de nada'. Não reconhece, porém, que é miserável, digno de
compaixão, pobre, cego, e que está nu.} ...

Repreendo e disciplino aqueles que eu amo. Por isso, seja diligente e
arrependa-se. Eis que estou à porta e bato. Se alguém ouvir a minha voz e abrir
a porta, entrarei e cearei com ele, e ele comigo.\footnote{Ap 3:15-17,19-20. Grifo
nosso.}
\end{quotation}

Este trecho partiu nossos corações em 2015 quando percebemos pela primeira vez que
Jesus estava falando essas palavras diretamente para nós, estudantes de MBA com
grandes planos para nossos futuros ganhos. Felizmente, Jesus não busca nos
condenar. Pelo contrário, ele pacientemente bate à porta, esperando que abramos
para que ele possa entrar e comer conosco. Se nos arrependermos de nossas atitudes
de independência financeira, poderemos desfrutar das verdadeiras riquezas de um
relacionamento profundo com ele, dependendo grata e humildemente de sua provisão
enquanto vivemos pela fé.

Não dependemos mais de nossa própria capacidade de ganhar dinheiro para sustento
na vida. Trabalhamos tão arduamente quanto antes, mas agora reconhecemos a Deus
como o provedor, agradecendo humildemente por trazer recursos para nós. Tudo é um
presente! Essa mentalidade gera uma incrível sensação de paz. Não há necessidade
de obcecar contando nosso dinheiro ou imaginar o que ganharemos no próximo ano,
quando, em vez disso, reconhecemos que Deus é a fonte de todas as coisas boas e
ele nos ama perfeitamente onde estamos hoje.

Imaginamos que o Rei Davi teve um momento semelhante de convicção após o censo. O
rei idoso pode ter repassado a fita em sua mente, entristecido por seu próprio
orgulho, e lembrado de um momento especial de sua infância.\footnote{Veja 1 Sm 16
para a base bíblica por trás da nossa interpretação dessa história.}

\begin{ideabox}
Quando alguém te elogia, você aceita diretamente o elogio ou atribui o mérito a
Deus? Atribuir honra a Deus por nossas boas ações é uma prática antiga do
cristianismo que cultiva a gratidão. Da próxima vez que alguém te elogiar, tente
atribuir o mérito a Deus!
\end{ideabox}

\begin{quotation}
``O profeta de Deus está aqui e pediu para nos ver!'', Jessé gritou para seus
filhos. ``Meninos, venham rápido!''

O profeta Samuel era uma lenda viva em toda Israel. O respeitado líder possuía uma
autoridade moral quase inimaginável e falava em nome de Deus. Por que ele estava
ali, naquela pequena cidade, em sua fazenda? Os líderes da cidade ficaram
aterrorizados a princípio, mas Samuel os tranquilizou, dizendo que veio em paz.

Um por um, Samuel cumprimentou os filhos de Jessé. Que honra para eles conhecerem
esse profeta de Deus! Mas Samuel parecia distraído... Algo estava errado. ``Estão
todos os seus filhos aqui?'', ele perguntou.

Jesse informou a Samuel que o mais novo, Davi, estava distante nos campos cuidando
das ovelhas.

``Chamem por ele!'', exigiu Samuel, esperando impacientemente enquanto um dos
irmãos corria para encontrar Davi. (Quando o profeta pedia algo, ele era
atendido.)

``Davi, venha aqui!'', seu irmão gritou, correndo para o pasto onde Davi estava
com as ovelhas. Ofegante depois de correr, ele disse ofegante: ``O profeta... de
Deus... quer te ver! Vá!''

Deixando seu irmão com as ovelhas, Davi correu de volta, sem fôlego, tendo perdido
toda a comoção anterior. Enquanto corria, ele tentava descobrir por que tinha sido
convocado. Estava em apuros? O que estava acontecendo? Ele viu a multidão a
algumas centenas de metros de distância e se aproximou correndo.

Um amplo e caloroso sorriso se espalhou pelo rosto de Samuel quando ele viu Davi.
``Venha aqui, jovem!''

Completamente confuso e ainda recuperando o fôlego, Davi percebeu que Samuel agora
estava pedindo para ele se ajoelhar. Mas por quê? Então Samuel começou a falar
sobre a unção de Deus estar sobre Davi e derramou um chifre de óleo sobre a cabeça
de Davi. Através do óleo escorrendo de seus ouvidos, Davi ouviu as palavras: ``...
ungi-lo como futuro rei de Israel''.
\end{quotation}

Nós imaginamos Davi recordando esse momento, sorrindo, lágrimas se acumulando em
seus olhos envelhecidos. Ele havia trabalhado duro e se esforçado em cada etapa da
vida, mas era inegável que Deus lhe havia dado tudo o que ele tinha. Um menino
pastor havia se tornado um grande rei.

Verdadeiramente, Deus é aquele que exalta os homens e os humilha, o mestre das
nações. É fácil se deixar levar por nossas conquistas e nossos grandes planos,
olhando orgulhosamente para o que mais podemos obter no futuro. Mas como
indivíduos que seguem a Deus, devemos sempre lembrar daquele que nos provê tudo o
que temos. E em vez de erguer nossas cabeças em orgulho, podemos escolher adotar
posturas de gratidão.
