\section*{Uma História de Esperança}

Nosso amigo Chris viveu sua própria versão da história do filho pródigo, bem aqui
no século XXI. Ele compartilhou o seguinte conosco:

\begin{quotation}
Dezoito meses atrás, desmaiei nas ruas de São Francisco em uma noite de
segunda-feira. Acordei debaixo de um ponto de ônibus, sem saber como cheguei lá,
perdido nas profundezas de um vício em álcool com vários anos. Eu estava bebendo
diariamente a ponto de não sentir mais nada, mal conseguindo manter a vida e o
trabalho juntos. Não havia muita esperança dentro de mim, apenas uma quantidade
crescente de dor, culpa e auto-ódio.
\end{quotation}

Para muitos que o conheciam de longe, isso foi uma surpresa. Chris era
excepcionalmente bem-sucedido na vida e um seguidor devoto de Jesus. Ele se formou
com seu MBA e tinha um ótimo emprego que pagava seis dígitos. Ele até havia
ocupado uma posição de liderança em um ministério universitário e em vários cargos
em sua igreja. Ele explicou que uma visão orientada pelo desempenho do mundo e de
sua própria fé o levaram a sucumbir ao peso da vida:

\begin{quotation}
Já que meu desejo de ter sucesso e agradar aos outros também se traduziu em
conquistas acadêmicas bem-intencionadas e desempenho religioso, acabei me
acostumando com a ansiedade e o medo que isso também nutria, o que, por fim,
alimentava a necessidade de me medicar com álcool e outras coisas para me sentir
bem.
\end{quotation}

Pensando que precisava conquistar seu lugar como cristão, ou se sair bem o
suficiente para agradar a Deus e aos outros, a vida se tornou insuportável e a
dependência começou a dominá-lo. Mas então, o amor de Deus irrompeu. Assim como
Davi sendo ungido por Samuel, assim como o pródigo se alegrando com o presente de
graça do pai, Chris encontrou o amor abundante de Deus de uma maneira totalmente
nova.

Nunca esquecerei de encontrar Chris em uma reunião de negócios e ouvi-lo
compartilhar, radiante de vida, que Deus havia despertado nele um senso de
reverência, maravilha e confiança de que seu Pai celestial o estava buscando com
um amor que nunca para, mesmo durante os anos de errância. Ele estava jubilante,
tendo experimentado Deus não como um juiz irado, mas como um Pai amoroso que o
esperava voltar para casa. Sua vida e as coisas que Deus lhe pediu para cuidar não
dependiam de Chris fazer tudo certo por conta própria. Conforme ele aprendeu a
descansar na confiança do amor de seu Pai, grato por sua suficiência, descobriu
uma paz que nunca teve antes.

Chris se recuperou de seu vício, eventualmente deixando seu emprego no Vale do
Silício para fundar a OneStep, uma plataforma online que conecta indivíduos e
famílias a recursos inspiradores, educacionais e de tratamento relacionados à
recuperação e saúde mental. A OneStep é estruturada para doar a grande maioria dos
lucros, para que outros possam descobrir o valor que Deus lhes deu e viver vidas
de saúde e propósito por meio da recuperação. Todo orgulho orientado pelo
desempenho que Chris tinha antes foi apagado, e agora sua gratidão transborda em
generosidade. Chris disse:

\begin{quotation}
A cada dia, acordo beliscando-me, sabendo que me foi dada não apenas outra chance
na vida, mas também a oportunidade de estar plenamente vivo de uma maneira que eu
não sabia que era possível. Quando fica claro o quanto cada dia de vida é um
presente, torna-se mais fácil oferecer tempo, vida e recursos.
\end{quotation}

A história de Chris é dramática, mas ilustra o que está em jogo na vida de cada
pessoa. A decisão mais importante que você tomará é reconhecer ou não e responder
ao amor de Deus por você, entregando alegremente sua vida ao seu direcionamento,
em vez de tentar fazer tudo acontecer por conta própria continuamente. A rendição
caminha de mãos dadas com a salvação. Quando você aceita o presente da salvação,
toda a sua vida passa a estar sob a autoridade e orientação de Deus, inclusive o
seu dinheiro.

Se você ainda não entregou sua vida a Jesus, a gestão financeira deve ser a menor
de suas preocupações, e encorajamos você a explorar profundamente Deus antes de se
envolver demais em aprender sobre uma administração financeira sábia. Se você já
entregou sua vida a Jesus, tudo pode ser reorganizado por uma gratidão
avassaladora, incluindo a forma como você pensa e administra o dinheiro.

Fundamentalmente, Deus quer nossos corações. Quando ele tem nossos corações,
financeiramente e de outras formas, comportamentos adequados fluem naturalmente.
Por causa disso, a obediência mecânica a uma regra não é o que ele busca, e é
totalmente inadequado simplesmente pegar a sabedoria financeira normal do mundo,
adicionar o dízimo (10\% de doação) e assumir que isso constitui a sabedoria
financeira cristã. Deus tem em mente uma aventura alegre, uma aventura que molda
não apenas o que fazemos, mas o que nos tornamos.

Como Jesus disse: ``O homem bom tira coisas boas do bom tesouro que está em seu
coração, e o homem mau tira coisas más do mal que está em seu
coração''.\footnote{Lc 6:45.} Tentamos nos perguntar regularmente: \textit{O que
está profundamente dentro do meu coração, por baixo dos meus comportamentos
financeiros?} Se nossos corações estão cheios de gratidão por quem Deus é e pelo
que ele nos deu, então estamos prontos para experimentar a alegria de Deus com
nosso dinheiro. Se nossos corações estão cheios de orgulho pelo que podemos
experimentar ou realizar, estamos em um caminho que tragicamente nos afasta das
verdadeiras riquezas da vida com Deus.

Moisés apontou explicitamente essa contraposição de orgulho e gratidão, chamando o
povo de Israel a lembrar-se da bondade de Deus como a fonte de suas bênçãos
crescentes. Ele aconselhou: ``Não digam, pois, em seu coração: `A minha capacidade
e a força das minhas mãos ajuntaram para mim toda esta riqueza'. Mas lembrem-se do
Senhor, o seu Deus, pois é ele que lhes dá a capacidade de produzir
riqueza.''\footnote{Dt 8:17-18. Antes desses versículos, Moisés descreve as
bênçãos que estão por vir: multiplicação de prata e ouro, novas casas, aumento na
produção agrícola e até mesmo uma próspera indústria de mineração de cobre.}

No final de sua vida, Davi abraçou o conselho de Moisés, que certamente
havia lido muitas vezes ao longo de sua vida, e passou a ter uma apreciação plena
dessa verdade.
