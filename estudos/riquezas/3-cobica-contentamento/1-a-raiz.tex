\section*{A Causa Fundamental da Dívida}

Talvez a evidência mais abrangente de nossa falta de contentamento seja o problema
da dívida do consumidor. Na maior economia mais próspera da história do mundo, os
Estados Unidos do século XXI, uma grande parcela da população luta para pagar suas
contas e, na verdade, fica cada vez mais endividada a cada mês. Como isso é
possível?

Talvez a prevalência da dívida do consumidor em nossa cultura seja apenas o
sintoma de um problema mais profundo e oculto. Poderia haver um padrão de pecado
tão difundido, tão normalizado, que somos cegos para ele, mesmo enquanto ele
consome nossas vidas e dificulta nossa capacidade de nos conectar e servir a Deus?

Como experimento mental, vamos imaginar uma comunidade cristã com pecado sexual
desenfreado e público. Imagine estar na entrada da igreja e ouvir um dos anciãos
da igreja se gabando de trair sua esposa. Confuso, você se afasta e então ouve um
amigo falando sobre flertar e fazer avanços sexuais em relação a um colega de
trabalho. Essa igreja poderia ser uma igreja fiel enquanto tolera um pecado tão
descarado?

Este exemplo parece ridículo, mas nossa sociedade rica cultivou exatamente esse
tipo de normalização pública e difundida de outro pecado: o pecado da cobiça.
Alguns de nós associam a cobiça com o desejo pelas coisas de outras pessoas. No
entanto, a cobiça não é necessariamente o desejo de roubar ou tirar algo de outra
pessoa. A cobiça, em sua essência, é simplesmente a crença de que se eu tivesse
mais, eu seria feliz. Pense nisso como se esforçar, ansiar, buscar inquietamente
mais. É uma forma de idolatria que nos afasta de Deus.

Você pode não ouvir as pessoas se gabando de pecado sexual no saguão da igreja,
mas provavelmente todos já ouvimos conversas banais sobre as coisas que queremos
experimentar ou comprar. Ficar insatisfeito com nosso carro e querer um novo,
conversar sobre a casa nova de fulano e como ela é bonita (e talvez o que eles
pagaram por ela), ou comparar planos de férias -- qualquer um desses tópicos
aparentemente inofensivos pode estar imerso na cobiça.

Pode ser fácil criticar os jovens estudantes da Harvard Business School, sonhando
se o ``número'' deles é de 10 milhões ou 70 milhões de dólares e debatendo os prós
e contras de possuir um jato particular. No entanto, quando sentimos inveja da
reforma da cozinha de um amigo ou um leve ciúme ao pensar que outra pessoa está
adquirindo o smartphone mais recente, somos culpados pelo mesmo pecado. Estejamos
perseguindo milhões de dólares ou apenas mais um dia de salário, cada um de nossos
corações humanos está propenso a cobiçar o que os outros têm, a estar
constantemente buscando mais do que nos é dado.

Aqui está algo fascinante que você pode observar ao estudar a Bíblia. Esse desejo
pecaminoso por mais, que afasta nossos corações de Deus, está presente tanto
quando cobiçamos bênçãos materiais quanto quando pecamos sexualmente. Na verdade,
o pecado sexual e a cobiça estão ligados em toda a Bíblia. Ambos são proibidos nos
Dez Mandamentos originais. E o apóstolo Paulo escreveu: ``Porque vocês podem estar
certos disto: nenhum imoral, ou impuro, ou ganancioso, que é idólatra, tem herança
no Reino de Cristo e de Deus.''\footnote{Ef 5:5.}

O livro de Hebreus apresenta um par de comandos em sequência imediata relacionados
ao sexo e ao dinheiro:

\begin{quotation}
O casamento deve ser honrado por todos; o leito conjugal, conservado puro; pois
Deus julgará os imorais e os adúlteros. Conservem-se livres do amor ao dinheiro
e contentem-se com o que vocês têm, porque Deus mesmo disse: ``Nunca o deixarei,
nunca o abandonarei''.\footnote{Hb 13:4-5.}
\end{quotation}

É fácil concordarmos que o pecado sexual flagrante e impenitente indica uma falta
de verdadeira fé ou devoção a Jesus. Mas a Bíblia diz que o mesmo é verdade para a
cobiça flagrante e impenitente. Ambos envolvem ignorar limites saudáveis em nossas
vidas humanas individuais. De fato, Ambrósio de Milão escreveu há mais de 1.600
anos que ``a única diferença entre [um homem ganancioso] e um adúltero é que um tem
um amor desmedido pela forma física e o outro, o desejo por uma fazenda, uma
propriedade rica''.\footnote{Ambrósio de Milão, em \textit{Caim e Abel}. Tradução
própria.}

Deus nos dá limites saudáveis em nossas finanças e em nossa pureza sexual. Violar
esses limites ao buscar satisfação sexual não santificada ou gastar em excesso é
uma rejeição do bom plano de Deus para a prosperidade humana. O estudioso da
Bíblia N.T. Wright, refletindo sobre a singularidade das primeiras comunidades de
Jesus no primeiro século, observou que ``A pureza sexual e a generosidade
financeira deveriam fazer parte do DNA cristão desde o início''.\footnote{N.T.
Wright, \textit{Paul: A Biography}. Tradução própria.}

Assim, sexo e dinheiro não são assuntos secundários para o povo de Deus, áreas da
vida às quais podemos submeter a Jesus quando nos convém. São questões centrais
para a ética de Cristo e sua igreja. Nossa cultura nos ensina a sermos generosos
com o sexo e a guardar nosso dinheiro com cuidado. Mas Deus nos ensina a sermos
generosos com o dinheiro e a guardar com cuidado nossa pureza sexual. É uma
inversão total -- que nos leva, em ambos os casos, a uma contentamento divino que
administra com sabedoria o que Deus nos deu!

Se você está lendo isso em uma sala com isolamento térmico e sabe de onde virá
sua próxima refeição, você está entre o grande número de pessoas que receberam uma
prosperidade incrível aqui no século XXI. Tendemos a dar por garantido nosso
acesso a alimentos, bebidas, uma cama confortável e roupas em abundância. No
entanto, mesmo em meio a essa abundância, fomos seduzidos pela busca de um estilo
de vida melhor e nos vemos lutando para doar sequer 10\% de nossa renda. A igreja
coletivamente orou: ``Dá-nos hoje o nosso pão de cada dia'', e Deus respondeu à
oração, confiando generosamente em nós fundos suficientes para comprar montanhas
de pão. Em vez de compartilhá-lo onde Ele pretendia, no entanto, nós nos saciamos
e depois usamos o que sobrou para adquirir casas melhores, carros, férias e
escolas particulares, até que não houvesse mais nada para compartilhar.

De acordo com Hebreus, esse pecado está na mesma categoria de levar uma vida de
promiscuidade sexual desenfreada. Vamos repetir isso. \textit{Gastar toda a nossa
renda em nós mesmos em busca de satisfação, em detrimento de nossa generosidade, é
como cometer adultério regularmente.} O que nos levou tão longe da visão de Deus
de um contentamento alegre? Vamos encarar isso. Somos cobiçosos e vivemos em uma
sociedade onde a cobiça é normalizada. Como o sapo na panela que só percebe que a
água está fervendo quando é tarde demais, estamos cercados por um ambiente
perigoso. Precisamos de alguém fora da panela para nos ajudar. Felizmente, nosso
Deus está pronto para ajudar e nos ofereceu um caminho de escape.

Deus nos convida a uma alegria maior -- um caminho longe da esteira da cobiça.
Como meu pastor disse: ``Há duas maneiras de ser rico. Uma é ter muito dinheiro, e
a outra é simplesmente não precisar de muito dinheiro.'' Este capítulo trata do
convite de Deus para nos tornarmos ricos da segunda maneira, através da alegria do
contentamento.
