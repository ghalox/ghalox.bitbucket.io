\section*{O Alegre Contentamento no Sofrimento}

Paulo aprendeu seu contentamento com Jesus, assim como milhões de cristãos têm
feito desde então. O sofrimento e a escassez, seja financeira ou de outra forma,
caracterizaram a vida de Jesus e eram comuns entre seus primeiros seguidores.
Apesar dessa tendência, os cristãos praticavam o contentamento alegre como um
marco de sua fé. Como disse Wess Stafford, presidente emérito da
\textit{Compassion International}: ``A alegria é uma decisão, uma decisão muito
corajosa, sobre como você vai \textit{responder} à vida''.\footnote{Wess Stafford,
em \textit{Too Small to Ignore: Why the Least of These Matters Most} (Colorado
Springs: Waterbrook Press, 2007), 165 (grifo original). Tradução própria.}

Na verdade, nosso alegre contentamento pode produzir um efeito em cascata. Os
cristãos da igreja de Tessalônica ``apesar de muito sofrimento, receberam a
palavra com alegria que vem do Espírito Santo'', de acordo com a correspondência
de Paulo. Ele lhes disse: ``Assim, tornaram-se modelo para todos os crentes que
estão na Macedônia e na Acaia.''\footnote{1 Ts 1:6-7.} Os tessalonicenses poderiam
ter escolhido permanecer em seu sofrimento, em sua carência, e desejar uma
realidade diferente, como todos nós fazemos às vezes. Em vez disso, eles abraçaram
a alegria do Espírito Santo, e a notícia de seu contentamento alegre alcançou até
mesmo a igreja na Macedônia.

Cinco anos depois, Paulo descreveu o que os macedônios fizeram com o que
aprenderam da igreja de Tessalônica: ``No meio da mais severa tribulação, a grande
alegria e a extrema pobreza deles transbordaram em rica generosidade'' em
direção aos cristãos em Jerusalém, em seu momento de necessidade.\footnote{2 Co
8:2.}

Observe a reação em cadeia de impacto!

\begin{enumerate}
\item Os tessalonicenses escolheram fielmente a alegria em meio à adversidade e à
escassez.
\item Os macedônios aprenderam com isso e imitaram essa alegria semelhante a
Cristo no sofrimento.
\item Essa alegria permitiu que os macedônios, apesar de sua pobreza, dessem
generosamente.
\item Essa generosidade supriu as necessidades dos cristãos em Jerusalém.
\item Sua história nos inspira a dar hoje.
\item Nossa generosidade abençoa e inspira outras pessoas.
\item E assim por diante!
\end{enumerate}

Imagine uma cena no céu: um homem de Jerusalém, que se tornou cristão quando
influenciado pela igreja lá, encontra uma mulher de Tessalônica, que sofreu com
paciência e alegria.

``Você nunca soube disso'', ele diz, ``mas seu alegre contentamento em meio às
dificuldades levou a uma reação em cadeia de eventos que eventualmente incluiu
minha própria salvação. Obrigado!''

Ela começa a sentir-se sobrecarregada de gratidão, mas ele continua: ``Minha
família estendida planejou uma festa para você. Há cem mil pessoas aqui no céu
entre meus descendentes, ao longo de cinquenta gerações, que conhecem o Senhor por
causa da minha conversão, que resultou da sua obediência fiel''.

A mulher pode ter vivido uma vida humilde e nunca imaginou que alguém realmente a
notasse. Este momento seria uma de suas maiores recompensas. Nossa alegria fiel e
espírito contente produzem um efeito em cascata, cujos resultados talvez nunca
conheçamos completamente. Procuramos por resultados e influência aqui e agora, mas
Deus está agindo através de nações e gerações.

Muitos de nós podem enfrentar empregos que não gostamos, desemprego prolongado, a
perda de um ente querido ou uma doença debilitante. As más circunstâncias são
universais. Diz-se que todos estão saindo de uma crise, estão em uma crise ou
estão prestes a entrar em uma crise, e trocaríamos essas dificuldades por
situações menos dolorosas se pudéssemos. Mas como seguidores de Jesus, somos
chamados a abandonar todas as formas de cobiça. Então, quando esses momentos
chegarem, devemos nos perguntar: como responderemos? Se nosso valor, identidade e
confiança estão depositados em nosso Deus Pai, podemos responder com contentamento
alegre. Estamos jogando um jogo de longo prazo e sabemos que nossas circunstâncias
presentes são muito temporárias diante da eternidade. Temos confiança de que um
dia entraremos na plenitude da presença de Deus e que nossas épocas de escassez
serão apagadas. Essa perspectiva pode transformar nosso sofrimento em algo
incrivelmente poderoso: um testemunho da bondade de Deus mesmo em circunstâncias
ruins.

Um pastor americano visitou um país no exterior que está experimentando um
crescimento explosivo em sua população cristã. Ele disse: ``Eles têm um plano de
crescimento da igreja em cinco etapas. Todos os dias, cada cristão deve ler a
Bíblia, orar, falar de Jesus com alguém e esperar um milagre. Essas são as quatro
primeiras etapas e são incríveis. Mas o quinto objetivo é diferente:
\textit{desenvolver uma teologia do sofrimento}. Eles me disseram que Deus pode
agir de maneira mais poderosa através do sofrimento de seu povo do que por meio de
seus confortos, e eles querem estar prontos para sofrer bem, para a glória de
Deus''.\footnote{Como lembrado num sermão do Dr. Joel Hunter, em Northland Church,
2017.}

Essa é uma forma de pensar tão contrária à nossa cultura, que busca constantemente
mais conforto, mais provisão e menos dificuldades. É algo que pode ser difícil de
entender, mas talvez eles estejam no caminho certo. Em vez de buscar mais
segurança e conforto, eles vivem de forma simples, preparando seus corações para
as lutas que virão. De fato, outro de nossos amigos logo relatou uma história
incrível que demonstrava esse princípio em ação.
