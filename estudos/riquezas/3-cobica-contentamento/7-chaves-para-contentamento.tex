\section*{Duas Chaves para o Contentamento}

Seguir Jesus para sair da armadilha de sempre querer mais, da cobiça, nos leva à
riqueza de um contentamento que possibilita a generosidade. Mas como isso se
parece na prática? Vamos considerar algumas ideias que nos ajudam a superar o
constante desejo por mais e experimentar a paz financeira com Deus. A primeira é a
\textit{margem}, para aqueles de nós com rendas limitadas, e a segunda é o
\textit{suficiente}, para aqueles de nós com rendas altas.

\subsection*{A Margem}

Criar margem em sua vida significa garantir que você tenha dinheiro extra todos os
meses. A margem ocorre quando você gasta menos do que ganha. Consideramos a margem
como o principal princípio para uma gestão financeira bem-sucedida, porque sem ela
você não consegue doar ou economizar de forma consistente.

No entanto, se quisermos economizar e doar de maneira significativa, precisamos
fazer muito mais do que apenas gastar um pouco menos do que ganhamos. Precisamos
gastar muito menos do que ganhamos! Vamos supor que você pague 15\% em impostos e
tenha a meta de doar 15\% e economizar 15\%.

Neste exemplo simplificado, isso deixa apenas 55\% para gastos! Isso requer um
cuidadoso planejamento e controle financeiro para ser bem-sucedido \\[2em].

\begin{center}
\begin{tabular}{|l|r|}
\hline
Renda Total & 100\% \\
\hline
Impostos & -15\% \\
\hline
Economias & -15\% \\
\hline
Doação & -15\% \\
\hline
O que sobra para gastos & 55\% \\
\hline
\end{tabular}
\end{center}

A margem é o presente de Deus para pessoas com rendas limitadas: uma forma de
planejar o futuro e planejar a generosidade, mesmo quando não somos ricos. De
fato, Deus \textit{ordenou} aos agricultores pobres de Israel que mantivessem
margem para que pudessem ser doadores.

\begin{quotation}
Quando fizerem a colheita da sua terra, não colham até as extremidades da sua
lavoura, nem ajuntem as espigas caídas de sua colheita. Não passem duas vezes
pela sua vinha, nem apanhem as uvas que tiverem caído. Deixem-nas para o
necessitado e para o estrangeiro. Eu sou o Senhor, o Deus de vocês.\footnote{Lv
19:9-10.}
\end{quotation}

Naquela época, a produção agrícola era essencialmente equivalente a dinheiro. Um
agricultor eficiente e diligente queria aproveitar até às últimas uvas!
Portanto, era um sacrifício, uma prática intencional de contentamento e
generosidade, seguir o mandamento de Deus. Se esse trecho fosse escrito hoje,
diria algo como: ``Mantenha margem em suas finanças. Não gaste cada dólar, mas
deixe dinheiro livre para que você tenha disponível para dar aos pobres.''

Quando vivemos no limite, gastando tudo o que ganhamos, a ideia de gastar menos é
desconfortável. Às vezes, a única maneira de gerar margem é abraçar grandes
mudanças de estilo de vida. Mas a margem é o único caminho para a saúde financeira
e para a fidelidade cristã. Melhor ainda, é o caminho financeiro para a paz e
alegria. Se você está lutando com essa ideia, faça uma pausa agora e ore. Cristo
pode lhe dar a força necessária para agir em busca da margem em sua vida.
Exploraremos passos tangíveis que você pode tomar na seção de aplicação deste
capítulo.

\begin{ideabox}
Considere abrir uma conta de poupança especial com algumas centenas de reais
reservados para doações espontâneas. Assim, se você identificar uma necessidade e
Deus o incentivar a dar, você poderá agir livre e imediatamente, sabendo que já
tem o dinheiro separado para a generosidade.
\end{ideabox}

\subsection*{O Suficiente}

Se Deus provê a alguém um alto rendimento, torna-se crucial descobrir o que é o
``suficiente''. Um cristão que está contente biblicamente e se esforça para ser
generoso não buscará constantemente aumentar seu estilo de vida. Quando
descobrimos o valor de Deus e do Seu reino, nossos desejos se deslocam de aumentar
nosso estilo de vida para aumentar nossa contribuição para a obra de Deus.

Não há nada de errado em aspirar a um emprego melhor, buscar uma moradia estável
ou esperar por um carro confiável. É perfeitamente ok ter coisas boas e desfrutar
delas como bênçãos de Deus. Você pode viver uma vida melhor aos quarenta anos do
que quando era estudante universitário. No entanto, Jesus advertiu sobre os
perigos da busca contínua por mais sem limites.

Muitas das famílias que admiramos no discipulado financeiro adotaram a noção de
``suficiente'' em seu estilo de vida. Elas sabem onde traçar a linha para si
mesmas e deixaram de se preocupar com a aquisição de mais brinquedos, experiências
ou metros quadrados. Para alguns deles, ``suficiente'' tem a seguinte cara:

\begin{itemize}
\item Um casal é proprietário de uma empresa bilionária e tem muitos filhos. Eles
    recebem um salário de US\$ 140.000 por ano, vivem em uma casa modesta e doam
    dezenas de milhões de dólares por ano.
\item Uma família que pode pagar uma escola particular de prestígio opta por
    enviar seus filhos para uma escola cooperativa menos cara como um ato de
    mordomia fiel, para que possam doar a diferença.
\item Um jovem empreendedor inicia uma empresa do zero, e ele e sua esposa se
    esforçam e diligentemente pagam suas dívidas. Quando o negócio tem sucesso e a
    renda deles atinge US\$ 1 milhão por ano, eles ainda escolhem viver de forma
    simples em uma casa modesta com quatro quartos e doar generosamente.
\end{itemize}

O objetivo de estabelecer um limite não é tornar-se ``o justo'' ao seguir alguma
regra aleatória, nem nos capacitar a julgar os outros que gastam mais do que nós.
Na verdade, uma mentalidade de pobreza também é pecaminosa. O Antigo Testamento
ordena muitas festas e celebrações, e Jesus festejava com seus discípulos. Uma
viagem bem programada ou uma sala de estar lindamente decorada podem ser
prazerosas e dignas de celebração. É fácil para muitas pessoas, inclusive para
nós, tornarem-se excessivamente rigorosas em relação aos cônjuges ou outros amigos
próximos, criticando os menores erros de gastos. Isso não nos torna sábios ou
frugais. Isso apenas nos torna desagradáveis!

Assim, o equilíbrio que buscamos aqui é a simplicidade alegre e cheia de graça. Se
você tem uma renda alta, seria sábio fazer sua família de cinco pessoas viver em
um antigo apartamento de estúdio para que possa doar mais de sua renda?
Provavelmente não. Mas nunca conhecemos alguém que tenha feito isso. Em nossa
cultura, geralmente não precisamos nos proteger de gastar menos; precisamos de uma
proteção contra o aumento do estilo de vida. Lembre-se de que Jesus advertiu que
alguns dos seus possíveis seguidores seriam impedidos pela ``preocupação desta
vida e o engano das riquezas'', tornando suas vidas ``infrutíferas''.\footnote{Mt
13:22.}

Os carros fornecem uma ilustração muito útil. Não havia carros nos dias de Jesus,
mas ele entrou em Jerusalém montado em um jumento, o meio de transporte dos
simples e dos pobres. Isso indicava que ele era um rei que vinha em paz e
simplicidade, em vez da grandiosidade de guerra de um cavalo ou carruagem. Um
humilde jumento era suficiente para o nosso Cristo. Isso levanta a questão: em uma
era em que pessoas financeiramente bem-sucedidas dirigem carros de luxo, os
cristãos devem seguir o mesmo caminho, ou devemos seguir o exemplo do nosso
Senhor, escolhendo simplicidade e humildade em nosso transporte?

Conhecemos um CEO cristão que, por muitos anos, dirigiu um carro velho e surrado
cujo velocímetro funcionava apenas esporadicamente. E outro que sempre compra
carros usados e se recusa a gastar mais de US\$ 15.000 em um. E um líder de
ministério internacionalmente conhecido que dirige um modesto Toyota Camry com
muita quilometragem. Nenhuma dessas pessoas está passando necessidades, mas todas
elas rejeitaram o jogo de \textit{status} dos carros para focar seu tempo, atenção
e afeições em coisas de maior importância e valor. Nossa escolha de carro é uma
oportunidade fantástica para modelar a humildade simples de Jesus.

Nos carros e em toda a vida, propomos que todos parem de perguntar: ``Quanto
preciso doar?'' e comecem a perguntar: ``Quanto realmente preciso guardar?'' Para
nós, pessoalmente, nossas famílias decidiram que não gastaremos mais de US\$
100.000 por ano, não importa qual seja nossa renda, liberando qualquer excesso
além desse valor para ser investido no reino de Deus.\footnote{Enquanto uma
família de quatro pessoas e sujeito a ajustes pela inflação com o tempo.} Para
deixar claro, US\$ 100.000 proporciona um estilo de vida muito bom; novamente, a
mordomia bíblica não se resume a uma competição de economia. Fixamos em US\$
100.000 porque esse valor nos permite oferecer um estilo de vida agradável para
nossas famílias sem ceder ao luxo apenas pelo luxo. Outras famílias chegarão a
números diferentes, e desafiamos todas as famílias a dedicarem tempo para
considerar qual número faz sentido para elas.\footnote{Fatores-chave que podem
impactar a decisão da sua família sobre o que é ``suficiente'' incluem: a região
geográfica onde você vive, o número de dependentes (filhos, pais idosos, etc.)
pelos quais você é responsável, estado de saúde, hobbies e interesses, entre
outros. Não existe uma fórmula única que forneça a resposta para cada família, mas
acreditamos que Deus o guiará fielmente para o número correto para a sua família
se você se envolver com Ele nesse assunto.} Não importa se esse número for US\$
50.000, US\$ 150.000 ou outro valor. O importante é enfrentar a pergunta com a
qual muitos de nós nos debatemos para responder: ``Quanto é 'suficiente'?''

No total, John atualmente ganha de US\$ 150.000 a US\$ 200.000 por
ano.\footnote{Nem tudo isso vem do salário da organização sem fins lucrativos.
Isso também inclui royalties e adiantamentos de livros, honorários por palestras e
sermões, e rendimentos de participação em conselhos.} Após considerar impostos,
economias diligentes e doações, restam cerca de US\$ 80.000 para gastar anualmente
-- não os US\$ 100.000 completos que teoricamente seriam o máximo com base no
compromisso final de sua família. Assim, sua família ainda não atingiu a linha de
chegada dos gastos, mas está satisfeita com seu estilo de vida atual mesmo assim.
Greg ganha significativamente mais no momento e poderia gastar além do valor da
linha de chegada, mas ainda poupa e doa bastante. Apesar dessa capacidade de
gastar, sua família optou por limitar seus gastos ao valor de US\$ 100.000 da
linha de chegada, reconhecendo que Deus tem provido mais do que suficiente para o
bem-estar deles, e o extra é destinado ao reino de Deus.

Estabelecer um limite para o estilo de vida pode garantir frutificação. Não é um
mandamento das Escrituras, mas é uma prática recomendada que implementamos em
nossas vidas na busca pelas verdadeiras riquezas. Em última análise, a vida é mais
do que gastar dinheiro, e nossos gastos devem sempre enfrentar uma restrição,
conforme indicado no gráfico abaixo:

\begin{center}
\begin{tabular}{|m{0.25\textwidth}|m{0.6\textwidth}|}
\hline
\textbf{Minha Renda É...} & \textbf{Meus Gastos Devem Ser Limitados Por...} \\
\hline
Modesta & A necessidade de \textit{margem}, para que eu tenha uma folga financeira
\\
\hline
Alta & A necessidade de reconhecer o \textit{suficiente}, para evitar distrações e
falta de frutificação \\
\hline
\end{tabular}
\end{center}

Se não somos abastados, nossos gastos serão limitados pela necessidade de margem.
Evitaremos gastar toda a nossa renda, porque queremos deixar espaço para poupar e
doar. No entanto, se alcançarmos uma relativa riqueza, torna-se possível aumentar
nosso estilo de vida em direção ao maior conforto, mesmo mantendo uma margem
saudável. Isso pode parecer uma bênção, mas também é um jogo extremamente
enganador e perigoso. Sem um conceito saudável de ``suficiente'', corremos o risco
de ser sufocados em nossa frutificação como cristãos pela busca de brinquedos,
distrações e luxos. Luxos não são necessariamente ruins, mas para os cristãos são
uma distração arriscada e devem ser considerados como exceção, e não como regra.

À medida que a família Cortines começou a experimentar um novo nível de renda
financeira em 2017, estabelecemos o objetivo de aumentar nossa margem e viver com
``o suficiente''. Queríamos doar US\$ 20.000. Foi um número assustador, mas cheio
de fé, para nós. No final do ano, quando somamos os números, tínhamos doado US\$
36.494! Isso representava 23\% de nossa renda bruta total. De alguma forma,
nossas finanças estavam mais saudáveis do que nunca, mesmo depois de toda essa
doação. Nosso estilo de vida não havia mudado, Deus havia trazido aumento, e nossa
doação atingiu novas alturas à medida que Deus abriu portas de oportunidade. Foi
incrível! Nós adoraríamos um carro novo, e Deus não nos amaria menos se
comprássemos um. Mas ficamos radiantes por ter dado aqueles fundos enquanto
praticávamos um estilo de vida estável e contente.

Quando nossa renda é menor, limitamos nossos gastos para alcançar \textit{a
margem}. Quando nossa renda é maior, limitamos nossos gastos para honrar o
princípio do \textit{suficiente}. Esses dois princípios nos ajudarão a seguir o
plano de Cristo para o contentamento em nossas vidas e, aos poucos, nos libertarão
dos hábitos de cobiça.
