\section*{A Esperança na Escuridão}

Sentado de pernas cruzadas no chão de terra, do outro lado do mundo, nosso amigo
comia uma tigela modesta de mingau que seus anfitriões haviam lhe dado. Eles
usavam coberturas faciais, então ele só podia ouvir suas vozes. ``Vocês não vão
comer?'' ele perguntou a eles.

``Não. Estamos jejuando. Nosso sofrimento começa no próximo mês.''

Seus anfitriões eram cristãos que haviam escapado de seu país opressor e
anti-cristão em busca de treinamento espiritual e teológico. Ele estava
visitando-os por apenas um dia e acabara de se juntar a eles em seu retiro secreto
e rural.

``O que você quer dizer com `sofrimento'?''

``Nós queremos voltar para casa e ensinar e treinar nossos irmãos e irmãs em
Cristo. Quando voltarmos, nossa ausência prolongada do país sem visto nos trará
problemas, e seremos punidos. Seremos presos, espancados, torturados e
interrogados. Isso vai durar meses ou anos. Depois disso, nos deixarão ir e
poderemos exercer nosso ministério. Eventualmente, se formos descobertos, seremos
executados. Só então teremos descanso - descanso eterno em Jesus.''

Até mesmo seus véus, explicou mais tarde o anfitrião, eram usados em preparação
para o que estava por vir. Cada pessoa os usava para proteger a anonimidade dos
outros, para que depois, sob tortura, não cedam e identifiquem outros
participantes do treinamento. Apesar das circunstâncias sombrias, no entanto,
esses pastores em formação estavam em paz, resolutos e calmos. Não apenas eram
pobres financeiramente, mas também enfrentavam a ameaça iminente de tortura e
morte.

Nosso amigo mais tarde contou sobre essa visita com essas pessoas corajosas.
``Eles falam sobre o batismo de maneira diferente do que nós. Nós falamos sobre
sermos batizados para uma nova vida. Isso é verdadeiro e bonito. Mas nós
esquecemos da parte de morrer. O batismo simboliza a morte do velho
eu.\footnote{Rm 6:4.} A morte dos nossos próprios planos. A morte do conforto e da
segurança, se essas coisas atrapalharem o discipulado. Esses amigos falavam de
serem `batizados na morte de Jesus', e eles sabiam que isso poderia significar
literalmente a própria morte nas mãos dos opressores.''

Seus olhos se encheram de lágrimas enquanto ele continuava. ``Eles nunca serão
beatificados como santos pela Igreja Católica. Nunca serão convidados para falar
em megatemplos protestantes. Eles são anônimos, e nenhuma fama terrena virá em seu
caminho. Mas eu nem mesmo sou digno de sentar aos seus pés. Precisamos da
perspectiva deles, da sabedoria deles, da devoção deles. Temos tanto a aprender
com eles.''

Enquanto ele compartilhava, eu (John) rapidamente me senti humilhado até o âmago.
Quero aprender a ter a paz contente desses líderes perseguidos. Quero depender de
Jesus tanto quanto eles! Talvez eu não seja chamado para ser preso por Jesus, mas
tenho a oportunidade de ampliar minha generosidade além de 15\% da minha renda
bruta. Esse tipo de esforço me faz preocupar com coisas como pagar minha casa
rápido o suficiente ou ser capaz de fazer as férias que eu gostaria, mas talvez
isso seja apenas a minha pequena forma de crescer em direção à generosidade
contente que Jesus nos chama. O martírio é o ato máximo de generosidade --
literalmente dar a própria vida por Deus. Talvez eu não me torne um verdadeiro
mártir, mas posso ser um sacrifício vivo, generoso em minha doação e alegremente
contente em todas as circunstâncias da minha vida.

Dietrich Bonhoeffer sofreu e eventualmente morreu nas mãos dos nazistas, mas ele
proclamou fielmente o evangelho até o fim. Ele escreveu sobre a banalização da fé
cristã: como ela se tornou algo bom de se acreditar, um bom conjunto de moralidade
e uma fonte de conforto para quando morremos. Ele lamentou a perda do senso de que
a escolha de seguir Jesus é tudo ou nada, exigindo ação em nossa vida e em nossas
finanças também. Ele escreveu:

\begin{quotation}
Jesus diz que todo cristão tem sua própria cruz esperando por ele, uma cruz
destinada e designada por Deus. Cada um deve suportar sua parcela alocada de
sofrimento e rejeição. Mas cada um tem uma parcela diferente: alguns Deus
considera dignos da mais alta forma de sofrimento e lhes concede a graça do
martírio, enquanto outros ele não permite que sejam tentados além do que podem
suportar. Mas é a mesma cruz em todos os casos.\footnote{Dietrich Bonhoeffer, em
\textit{The Cost of Discipleship} (New York: Simon \& Schuster, 1995), 89.
Tradução própria.}
\end{quotation}

Jesus nos chama para seguir, e às vezes seu chamado significa perder nossas vidas,
perder nossas famílias ou sermos expulsos de nossas profissões. Às vezes,
significa algo menos extremo: um chamado para criar famílias piedosas, discipular
outros crentes e viver vidas simples e contentes marcadas pela generosidade em vez
da cobiça. A maioria de nós pode agradecer por não termos sido chamados a sofrer
tortura e sofrimento físico por causa de nossa profissão do evangelho. Mas temos
uma tarefa desafiadora e libertadora: o compromisso diário de silenciar a voz da
cobiça para que possamos viver de forma simples, trabalhar diligentemente e dar
generosamente.
