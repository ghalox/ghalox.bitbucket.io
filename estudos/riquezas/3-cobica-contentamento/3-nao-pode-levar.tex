\section*{Você não Pode Levar com Você}

À medida que gastamos menos e abraçamos a satisfação, paradoxalmente
experimentamos ganhos. A vida é melhor quando deixamos de cobiçar e paramos de nos
esforçar por mais. Paulo compartilhou essa mesma visão com seu jovem amigo e
pastor Timóteo:

\begin{quotation}
De fato, a piedade com contentamento é grande fonte de lucro, pois nada
trouxemos para este mundo e dele nada podemos levar; por isso, tendo o que comer
e com que vestir-nos, estejamos com isso satisfeitos. Os que querem ficar ricos
caem em tentação, em armadilhas e em muitos desejos descontrolados e nocivos, que
levam os homens a mergulharem na ruína e na destruição, pois o amor ao dinheiro
é a raiz de todos os males. Algumas pessoas, por cobiçarem o dinheiro,
desviaram-se da fé e se atormentaram com muitos sofrimentos.\footnote{1 Tm
6:6-10.}
\end{quotation}

Paulo está indo diretamente contra o \textit{ethos} americano aqui. O desejo de
ser rico é insensato, prejudicial e uma raiz de muitos males -- uau! Paulo
trabalhou arduamente em uma profissão respeitável como fabricante de tendas e em
outros lugares encorajou as pessoas a cuidarem de suas famílias.\footnote{1 Tm
5:8.} Ele claramente não era contra o trabalho, ele era apenas contra as
distrações. Ele estava reforçando a perspectiva de Jesus de que a vida é mais do
que dinheiro e posses, e que as coisas que cobiçamos fazem promessas vazias. Elas
não podem nos satisfazer. Como um amigo disse recentemente: ``Finalmente comprei o
carro que eu queria, e para minha surpresa, de repente senti medo e restrição, não
liberdade e alegria''.

O desejo ávido por riquezas, alimentado pela cobiça, dominou o Egito durante o
período em que o povo de Israel estava escravizado lá. Deus os libertou desse
sistema no êxodo, assim como ele quer nos libertar. O mundo de Faraó era um mundo
de escassez e de tomar. Se você não tomasse comida, trabalho e riqueza dos outros,
eles seriam tirados de você. Os fortes governavam e os fracos serviam. Deus tirou
Israel da economia de Faraó e os trouxe para a sua própria economia de abundância.
O maná -- pão do céu -- estava disponível todos os dias. Deveria haver igualdade,
compartilhamento, comunidade e amor. Os fortes deveriam cuidar dos fracos, e todos
deveriam acolher o estrangeiro. Havia até um dia para não trabalhar, o sábado, um
dia para quebrar o poder da cobiça sobre o coração humano.

Como o teólogo Walter Brueggemann disse: ``Se traçarmos... desde Adão até Moisés,
podemos sugerir que a história central é uma história sobre
cobiça''.\footnote{Walter Brueggemann, em \textit{Money and Possessions}
(Louisville: Westminster John Knox Press, 2016), 15. Tradução própria.} Mas Deus
veio para romper essa história central, ensinando ao seu povo que somente Ele era
a sua suficiência e que ter mais coisas nunca traria plenitude. Infelizmente, com
muita frequência tentamos deixar a terra prometida e voltar ao Egito, nos
acorrentando à busca interminável por mais.

De fato, o antigo livro de Eclesiastes dedica muito tempo desconstruindo a busca
por riqueza e posses, demonstrando o quão fútil ela é. O autor observou que ``Quem
ama o dinheiro jamais terá o suficiente; quem ama as riquezas jamais ficará
satisfeito com os seus rendimentos. Isso também não faz sentido''.\footnote{Ec
5:10.} Buscar dinheiro ou posses por si mesmos é uma forma trágica de passar nosso
tempo.

Ninguém deseja em seu leito de morte ter acumulado mais riqueza ou comprado uma
casa maior. Eles desejam ter passado mais tempo com sua família ou ter vivido uma
vida com mais significado. Uma amiga resumiu isso muito bem depois que seus pais
idosos faleceram: ``Passei o dia inteiro ontem limpando as coisas dos meus pais da
casa deles. Por que nos apegamos tanto a todas essas posses? Por que eles tinham
todas essas coisas? As peças voltam para a caixa quando o jogo acaba''.

Quando Paulo escreveu sobre piedade e contentamento, não era apenas uma teoria
vazia. Ele tinha vivido isso. Você conhece aquele famoso versículo da Bíblia:
``Tudo posso naquele que me fortalece''? Esse versículo é frequentemente usado por
atletas que competem em eventos esportivos. Ele inspirou inúmeras pessoas jovens a
trabalhar mais para aperfeiçoar suas habilidades, mas será que esse versículo foi
realmente destinado a isso?

Quase todo mundo já ouviu esse versículo, mas poucas pessoas sabem que Paulo o
escreveu sobre dinheiro e estilo de vida. Ele não escreveu essas famosas palavras
depois de vencer um jogo; ele as escreveu na prisão, depois de receber um grande
presente financeiro.

\begin{quotation}
Não estou dizendo isso porque esteja necessitado, pois aprendi a adaptar-me a toda
e qualquer circunstância. Sei o que é passar necessidade e sei o que é ter
fartura. Aprendi o segredo de viver contente em toda e qualquer situação, seja bem
alimentado, seja com fome, tendo muito, ou passando necessidade. Tudo posso
naquele que me fortalece.\footnote{Fp 4:11-13.}
\end{quotation}

Quando a igreja de Filipos soube do aprisionamento de Paulo, eles enviaram um
mensageiro em uma jornada longa e arriscada para lhe trazer um generoso presente
de provisão. Esse presente pode ter incluído um casaco quente para noites frias,
juntamente com fundos para melhorar sua modesta dieta de prisioneiro. Paulo
escreveu essas palavras para deixá-los cientes de sua imensa gratidão. Mas ele
também queria que eles soubessem que sua alegria já estava garantida antes do
presente, que a alegria em Cristo não depende das circunstâncias.

Paulo havia passado fome, naufragado, sido espancado com varas, açoitado,
encarcerado e tremeu de frio em noites de exposição. Ele também havia
compartilhado refeições com elites, desfrutado dos benefícios da prestigiosa
cidadania romana e recebido uma educação de nível universitário.\footnote{Paulo
havia sido um aluno exemplar sob o tutorado do rabino mais famoso de sua época,
Gamaliel.} Ele sabia como ser alegremente rico e como ser alegremente pobre. O
segredo de ambos é simplesmente Cristo. Ele sabia que o contentamento é um estado
de espírito em Cristo, não um estado de situações no mundo.
