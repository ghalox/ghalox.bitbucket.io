\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[brazil]{babel}
\usepackage[a4paper]{geometry}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{indentfirst}

\title{O Sermão do Monte \\
\normalsize{Um pequeno guia de estudos (em construção)}}

\addto\captionsbrazil{%
    \renewcommand{\contentsname}%
        {}%
}

\author{Igreja Presbiteriana Chinesa de Curitiba}
\date{Fev--Mar/2021}

\begin{document}
\maketitle

\tableofcontents

\pagebreak

\section{Introdução ao Sermão do Monte}

O Sermão do Monte é a primeira das cinco grandes pregações de Jesus
no livro de Mateus (as quais estão em Mateus 5-7, 10, 13, 18 e 24-25).
Antes do Sermão do Monte, Mateus diz que Jesus pregava o evangelho do reino,
dizendo: ``Arrependam-se, pois o Reino dos Céus está próximo!''.
Esse sermão é a primeira oportunidade que temos no livro de Mateus
de entender com mais detalhes que reino era esse que estava sendo anunciado
e o que Jesus queria dizer por \emph{arrependam-se}.

É importante lembrar que na época em que o livro de Mateus foi escrito
o acesso a livros era muito difícil; e boa parte deles foi escrito não
para ser lido, mas para ser \emph{ouvido} e então guardado no coração.
O mesmo se aplica ao Sermão do Monte! E é seguindo por esse princípio
que iremos conduzir este estudo.

\subsection*{Atividade}
Vamos ler o Sermão do Monte na íntegra, juntos!
Naturalmente, é interessante avisar que é isso o que vamos fazer:
ler o texto inteiro e depois conversar sobre as primeiras impressões
que tivemos. Vamos conversar mais a fundo sobre cada parte nos
encontros seguintes.

Prepare uma cópia do texto do Sermão do Monte sem a divisão
de capítulos e versículos. Divida o texto em blocos coerentes
e peça para que cada pessoa leia um ou mais blocos.
Uma leitura tranquila leva por volta de 20 a 30 minutos.

\pagebreak

\section{As boas-aventuranças e o chamado de Cristo (5:1-16)}

Uma introdução tem muitos objetivos. Um deles é estabelecer alguns pontos
e preparar os ouvintes para o que vem depois. Hoje vamos dar uma olhada
na introdução do Sermão do Monte e buscar enxergar o que ela estabelece
em preparação para o resto da mensagem.

\subsection*{Reflexão 1}

Pare e pense:
\emph{O que dizem que precisamos ser/fazer para viver bem, hoje em dia?}\\

\indent (Alguns exemplos: fazer exercícios, dormir regularmente, fazer
terapia, ir pra faculdade, se alimentar direito, entender qual é o seu
lugar de fala, ter muitos amigos, se desafiar, viajar o mundo, curtir a vida,
evitar brigas, se afastar de pessoas tóxicas, e por aí vai.)\\

Leia as boas aventuranças (Mateus 5:1-11). Você pode pedir
para que cada pessoa leia uma, ou que alguém leia todas.\\

Nesse trecho, Jesus faz uma lista de pessoas que ele considera afortunados
(dependendo da sua versão, ele os chama de \emph{bem-aventurados} ou
\emph{felizes}). Contudo, se olharmos com atenção, essas não são bem as
primeiras pessoas que vêm à nossa cabeça quando pensamos em quem está se
dando bem na vida.\\

Pare e pense:
\emph{Como você se sentiria se estivesse ouvindo essa primeira parte da
mensagem e se identificasse com algum dos bem-aventurados listados?}

\subsection*{Reflexão 2}

Leia o que Jesus fala sobre o sal da terra (Mateus 5:13).\\

O sal pode ser usado para várias coisas, mas o que o fazia realmente
importante na época de Jesus era que ele era usado para \emph{preservar
comida} (aliás, é por isso que se salga carnes como bacalhau ou carne-seca;
e mesmo hoje o sal ainda faz parte do processo de produção de muitas
conservas). A importância do sal era tão grande que os soldados romanos
eram pagos com sal; o que era chamado de \emph{salário}.

O sal daquela região tinha que ser escavado das minas de sal, de onde
ele vinha impregnado de outras substâncias. Quando o sal não tinha mais
sabor (isto é, quando só ficavam essas outras impurezas), ele também não
preservava mais a comida; mas ainda servia para uma coisa: evitar que
crescesse mato nas trilhas. Por isso é que eles o lançavam \emph{para ser
pisado pelos homens}.\\

Pare e pense:
\emph{O que Jesus quis dizer por ``Vocês são o sal da terra''?}\\

\indent (Uma interpretação possível é a de que ou preservamos o mundo,
impedindo-o de apodrecer, ou matamos a vida e somos dignos de desprezo.)

\subsection*{Reflexão 3}

Leia o que Jesus fala sobre a luz do mundo (Mateus 5:14-16).\\

\begin{enumerate}
    \item O que Jesus quis dizer com ``Não se pode esconder uma cidade
        construída sobre um monte''?
    \item Quando dizem que ``é a sua vez de brilhar'', o que vem à nossa
        cabeça? Como isso se compara ao que Jesus está falando sobre
        ser luz?
    \item O que costuma acontecer quando vemos as boas obras de alguém?
        O que Jesus esperava que acontecesse quando as obras da luz do mundo
        fossem vistas pelos homens? O que levaria alguém a reagir dessa
        maneira?
\end{enumerate}

\subsection*{Reflexão 4}

Agora que refletimos um pouco sobre as partes do que Jesus estava falando,
podemos olhar para esse trecho como um todo.

\begin{enumerate}
    \item Nesse trecho, Jesus diz que ``vocês'' são bem-aventurados,
        o sal da terra e a luz do mundo. Quem são esse ``vocês''?
    \item Se você estivesse lá escutando o discurso dele, o que você
        estaria pensando ou sentindo, depois de tudo o que ele falou
        até agora?
    \item Jesus escolheu começar o sermão dele descrevendo ``vocês''
        dessa maneira. Por que será?
\end{enumerate}

\pagebreak

\section{Uma Justiça Superior (5:17-48)}

No último encontro, vimos Jesus chamando algumas pessoas (os pobres em
espírito, os que choram, os humildes, ...) e então motivando elas, como
se dissesse: ``O Reino dos céus está próximo; e é de vocês!''. A um grupo
que poderia se sentir impotente para fazer qualquer diferença, ele chamou
de ``sal da terra'' e ``luz do mundo''. \emph{Brilhe a luz de vocês diante
dos homens, para que vejam as suas boas obras e glorifiquem ao Pai de vocês,
que está nos céus}, ele lhes disse.

É para esse público, dos bem-aventurados que herdarão a terra, que Jesus
lançou o desafio que começaremos a ver hoje. Vejamos o que ele falou.

\subsection*{Reflexão 1}

Pare e pense: \emph{O que é e para que serve uma lei?}

(Algumas respostas possíveis são: uma regra de conduta para que a sociedade
funcione, um padrão mais objetivo para se avaliar o que é justo, ...)\\

Leia Mateus 5:17-20.\\

Existem alguns termos que precisamos explicar, aqui.

\emph{A Lei e os Profetas} era uma maneira de se referir à coleção completa
dos livros da Bíblia que eles tinham na época. Em particular, a Lei se referia
aos cinco primeiros livros da Bíblia (Gênesis, Êxodo, Levítico, Números e
Deuteronômio), que incluem, dentre outras coisas, a base para as leis judaicas
civis e cerimoniais.

\emph{Fariseus e mestres da lei} eram dois grupos de experts da Lei. Os
\emph{fariseus} eram um grupo que enfatizava a importância da
\emph{tradição judaica} na prática da fé: além da Bíblia, eles também
se pautavam fortemente num conjunto de entendimentos, práticas e costumes
que foram transmitidos de geração em geração, em boa parte oralmente.
Por isso, eles criam, por exemplo, na existência de anjos e demônios,
na ressurreição de mortos, na vinda de um messias e na vida após a morte.

\emph{Mestres da lei}, por sua vez, costuma também ser traduzido como
\emph{escribas}. Eram eles quem faziam cópias da Bíbia, naquela época.
Lembre-se que naquela época saber ler não era uma habilidade comum; e por isso
aqueles que tinham contato próximo com a Bíblia costumavam também ser pessoas
que explicavam a Lei para as multidões. A sua ênfase acabava sendo limitar-se
no que estava de fato escrito na Lei e nos Profetas.\\

Pare e pense:
\emph{O que Jesus quis dizer com ``a justiça de vocês deve ser superior à dos
fariseus e mestres da lei''?}

(É apenas uma pergunta para instigar a reflexão. A seguir veremos
muitos e muitos exemplos do que ele quis dizer com isso.)

\subsection*{Reflexão 2}

Vamos ler juntos alguns exemplos do que Jesus estava querendo dizer.
Cada um dos trechos que leremos começa com uma citação a um trecho da Lei;
e Jesus prossegue falando como seria uma justiça que supera à dos fariseus
e mestres da lei. Antes de ver as explicações, vamos primeiro ouvir sobre
que trechos Jesus falará.\\

Mostre a seguinte lista para todos, e peça para que alguém a leia com calma.

\begin{enumerate}
    \item Não matarás.
    \item Não adulterarás.
    \item Aquele que se divorciar de sua mulher deverá dar-lhe
        certidão de divórcio.
    \item Não jure falsamente, mas cumpra os juramentos que você fez
        diante do Senhor.
    \item Olho por olho e dente por dente.
    \item Ame o seu próximo.
\end{enumerate}

(A título de informação, essas leis estão, respectivamente, em
Êx 20:13, Êx 20:14, Dt 24:1, Lv 19:12, Êx 21:24, Lv 19:18)\\

Certifique-se de que todos entenderam a ideia de cada uma dessas leis.

\subsection*{Reflexão 3}

Vamos então ler juntos o que Jesus falou sobre cada uma dessas leis.
Peça para que cada pessoa leia com calma cada um dos seguintes trechos.
Podemos fazer uma pausa em cada trecho para conversar um pouco sobre ele.

\begin{enumerate}
    \item Mateus 5:21-26
    \item Mateus 5:27-30
    \item Mateus 5:31-32
    \item Mateus 5:33-37
    \item Mateus 5:38-42
    \item Mateus 5:43-47
\end{enumerate}

Jesus então finaliza esse pedaço dizendo:
``Portanto, sejam perfeitos como perfeito é o Pai celestial de vocês.''\\

Pare e pense:
\emph{Diante desses exemplos, o que Jesus tinha em mente ao dizer que
``a justiça de vocês deve ser superior à dos fariseus e mestres da lei''?
O que ele está querendo dizer com ``sejam perfeitos como perfeito é o Pai
celestial de vocês''?}

\indent (Parece que, segundo ele, só cumprir a lei não era o suficiente.
De certa forma, ele nos pede a dar passos a mais; e esses passos a mais
acabam cumprindo cada uma das leis. Ele não estava falando de ser mais justo
que os fariseus e os mestres da lei, mas sim de carregar consigo uma visão de
\emph{justiça} diferente, superior do que a que eles consideravam.
\emph{Justiça}, nesse caso, se estende para além da questão do que é certo ou
errado: trata-se também de viver plenamente uma vida aos moldes de Deus.)

\pagebreak

\section{Expressões de Justiça (6:1-18)}

No encontro anterior, vimos que Jesus nos lançou um desafio:
``Se a justiça de vocês não for muito superior à dos fariseus
e mestres da lei, de modo nenhum entrarão no Reino dos céus''.
Ele nos deu então alguns exemplos de como seria essa justiça
que ao mesmo tempo supera e cumpre a lei: sequer ter motivos
para matar, ou adulterar, ou jurar; mas ao invés disso abrir
mão da vingança e amar até aqueles que merecem o contrário de
nosso amor. Uma justiça que não girava em torno da interpretação
da lei, mas que buscava a sua viva essência.

Assim como nos dias de hoje, naqueles tempos as pessoas também
estavam cientes de que, para ser uma pessoa \emph{realmente} boa,
não bastava se limitar a não ser má: era necessário também que
ela desse o passo além de exercitar uma bondade ativa, seja na
forma de virtudes/caráter, seja na forma de gestos de bondade.
O trecho que estudaremos hoje trata desses gestos.

\subsection*{Reflexão 1}

Hoje vamos ver sobre três gestos que faziam parte da chamada
\emph{piedade judaica}. Era esperado que todo bom judeu que
era bom \emph{mesmo} praticasse essas três atividades. Elas são:
\begin{itemize}
\item \textbf{Caridade}: Praticada principalmente na forma de
    esmolas para os pobres, ela podia ser entendida como toda
    ajuda concedida para alguém em necessidade ou dificuldade.
\item \textbf{Oração}: Assim como nós, naquela época os judeus
    a qualquer hora expressavam de maneira espontânea seus
    pensamentos e sentimentos a Deus.
\item \textbf{Jejum}: O jejum é a prática de se abster de comida
    e bebida durante períodos de tempo para se aproximar de Deus.
    Ele deve ser acompanhado de orações e confissão de pecados.
    Na época de Jesus, o jejum comum era feito apenas durante o
    dia, enquanto em ocasiões ele podia durar plenas vinte e
    quatro horas. Além disso, era comum naquela época separar
    dois dias da semana para fazer jejum rotineiramente.
\end{itemize}

Pare e pense:
\emph{Por que esses três gestos são expressões de justiça?}

(Uma resposta possível é que todos podem ser consequências de uma
vida que busca viver aos moldes de Deus, que é a justiça que Jesus
nos chamou a viver, como vimos no encontro passado.)

\subsection*{Reflexão 2}

Jesus começa essa parte do sermão falando o que está em Mateus 6:1.
Leia Mateus 6:1. Isso soa um pouco familiar com uma parte do sermão
que já vimos. Releia Mateus 5:16.\\

Pare e pense:
\emph{Como resolver a contradição aparente entre esses dois trechos?}

(Num dos trechos, Jesus nos incentiva a praticar boas obras para que
as pessoas vejam e glorifiquem a Deus. No outro, ele nos desencoraja
a praticar boas obras com o fim de que as pessoas vejam e \emph{nos}
glorifiquem. Isso vai ficar bem claro nos trechos que vamos ler em
seguida.)

\subsection*{Reflexão 3}

Vamos então ver os exemplos que Jesus nos deu.
Leia os seguintes trechos. Após cada leitura, deixe o espaço livre para
comentar sobre o conteúdo de cada um deles.

\begin{itemize}
    \item Mateus 6:2-4
    \item Mateus 6:5-6 e Mateus 6:7-8
    \item Mateus 6:16-18
\end{itemize}

(Como observação, Mateus era um cobrador de impostos; e estamos lendo
o Sermão do Monte como relatado por ele. O termo \emph{recompensa} que
aparece nesses trechos é uma alusão a essa ideia de ``devida retribuição
por um gesto''; mas de forma alguma devemos entender os gestos de justiça
como uma transação com Deus ou com os homens. Uma maneira melhor de
interpretar esse trecho é entender \emph{recompensa} como \emph{resposta}
ou \emph{reação} a esses gestos. Se somos hipócritas, nosso Pai vê nossos
gestos e torce o nariz; e toda a resposta que recebemos vem de quem nos
louva à nossa volta. Mas se nosso gesto de fé é genuíno, Deus o recebe
de bom grado; e cultivamos assim essa nossa relação com ele.)

\subsection*{Reflexão 4}

Enfim, nessa parte do sermão que estamos considerando, Jesus também nos
ensina a oração do Pai Nosso. A própria oração poderia ter todo um estudo
dedicado somente a ela, mas como estamos vendo-a no contexto do sermão
como um todo, não nos aprofundaremos muito agora.\\

Leia Mateus 6:9-15.\\

Podemos ver essa oração mais como um guia de como podemos orar. Ela começa
nos lembrando de quem é Deus (``Pai nosso, que estás nos céus'') e com três
pedidos para Deus (``Santificado seja o teu nome. Venha o teu reino; seja
feita a tua vontade, assim na terra como no céu.''). Em seguida, temos três
pedidos para nós: que Deus nos dê o que precisamos para hoje, que nos perdoe
como perdoamos aos outros, que nos proteja de cair e de ser atacados pelo mal.
Enfim, ela termina com uma forte declaração sobre a soberania de Deus:
que dele é o Reino, o poder e a glória para sempre. E então um ``Amém'',
que é um termo hebraico que significa literalmente ``certamente'', ou
``verdadeiramente''; uma resposta em confirmação ao que foi dito.

\pagebreak

\section{Um Coração entre Dois Mundos (6:19-34; 7:1-12)}

Se formos observar as passagens que vimos no encontro anterior,
veremos que mais de uma vez Jesus observou que os hipócritas,
em seus gestos, ``já receberam a sua plena recompensa'' aqui na terra;
enquanto que ele mais de uma vez nos chamou para viver de tal forma
que ``seu Pai, que vê em secreto, os recompensará''.

No trecho que leremos hoje, Jesus vai seguir falando um pouco sobre isso:
sobre onde estamos depositando nossas expectativas, nossas esperanças,
nossos desejos, nosso coração.

\subsection*{Reflexão 1}

Jesus aborda o assunto de três perspectivas diferentes. Vamos ver o que
ele disse. Enquanto lemos as seguintes passagens, preste atenção nas
semelhanças e nas diferenças entre elas.

\begin{itemize}
    \item Mateus 6:19-21
    \item Mateus 6:22-23
    \item Mateus 6:24
\end{itemize}

Pare e pense:
\emph{Quais as semelhanças e as diferenças entre essas passagens?
Como elas se aplicam à nossa vida? Em que ocasiões elas são relevantes?}

(Jesus falou sobre onde consideramos estar nossas riquezas, tomar cuidado
com a maneira como olhamos as coisas, e prestar atenção a quem estamos
servindo. Cada uma das aplicações que leremos foca nesses aspectos.)

\subsection*{Reflexão 2}

Jesus segue com três passagens nas quais podemos aplicar esses princípios.
Leia Mateus 6:25-34. Como os trechos que lemos se aplicam a essa situação?
Nesse contexto, como esse ensino se aplica à nossa vida?\\

(Repare que o v. 34 é um alerta bastante interessante para alguém que
serve às riquezas. Você pode entender o v. 33 como um chamado para servir
a Deus, o qual sustenta a seus servos devidamente para seu trabalho.)

\subsection*{Reflexão 3}

Leia Mateus 7:1-6. Mais uma vez, se pergunte: Como os trechos que lemos
se aplicam a essa situação? Como esse ensino se aplica à nossa vida?\\

(Este trecho trata de como vemos. Ele explicitamente menciona olhos e
fala sobre enxergar. Ademais, esse trecho também fala sobre discernir,
principalmente na parte sobre não lançar pérolas para cães e porcos.
Ou, em outras palavras, não insista em fazer enxergar plenamente as
riquezas quem se recusa a vê-las e nem irá saber apreciá-las. (Mas ore
por eles e viva de modo que eles venham a se transformar))

\subsection*{Reflexão 4}

Enfim, leia Mateus 7:7-12. O que esse trecho tem a ver com os princípios
que vimos Jesus colocando para nós hoje? Como este ensino se aplica à
nossa vida?\\

(Lembramos que nosso Pai é alguém muito generoso; e dá de suas riquezas
com muita alegria a seus filhos. É refletindo essa generosidade que
somos chamados a também nós vivermos com essa mentalidade de abundância:
uma que não teme que falte e sabe que pode simplesmente pedir se precisar;
que se dispõe a dar a quem pede, porque sabe que não vai lhe faltar.
Isso faz mais sentido quando pensamos em alguém cujo tesouro está no céu;
e a conversa muda bastante quando consideramos alguém cujo tesouro está
na terra.)

\pagebreak

\section{Um Convite e Dois Caminhos (7:13-27)}

No encontro anterior, Jesus falou de três duplas:
tesouros da terra e dos céus, olhos bons e olhos maus, Deus e as riquezas.
Depois, ele passou algum tempo falando de como seria viver
tendo o nosso tesouro nos céus, com bons olhos, servindo a Deus.
Hoje veremos a última parte do Sermão, em que Jesus nos faz um convite
e menciona algumas duplas que partem desse convite.

\subsection*{Reflexão 1}

Leia e imagine o que Jesus diz em Mateus 7:13-14.\\

Mantenha a seguinte pergunta em mente:
\emph{Jesus nos convida a entrar pela porta estreita.
O que ele quis dizer com isso?}

\subsection*{Reflexão 2}

Leia e imagine o que Jesus diz em Mateus 7:15-20.\\

Mantenha a seguinte pergunta em mente:
\emph{O que Jesus quis dizer por ``pelos seus frutos os conhecereis''?}

\subsection*{Reflexão 3}

Leia Mateus 7:21-23.\\

Responda as duas perguntas que acabamos de fazer. (:

\subsection*{Reflexão 4}

Leia Mateus 7:24-27.\\

Para que serve a \emph{fundação} de um prédio?
Ela serve para firmar a construção ao seu terreno.
Dependendo do tamanho da construção e da firmeza do solo,
pode-se fazer uma fundação rasa, com menos de três metros
de profundidade. Contudo, se você realmente deseja firmar
o prédio, é necessário escavar bastante, para que possamos
instalar estacas profundas que nos servirão de fundação.\\

Pare e pense:
\emph{Quais as semelhanças entre aqueles que ouvem as palavras
sem praticá-las e alguém que constrói sua casa na areia?
Quais as semelhanças entre aqueles que ouvem as palavras
e as acatam e alguém que firma a sua casa na rocha?}

\subsection*{Reflexão 5}

Pare e pense:
\emph{Como é na prática a vida de alguém que é como quem construiu
a sua casa na rocha? Como ele fez para construir a sua casa nela?}

\pagebreak

\section{A Conclusão (7:28-29)}

\pagebreak

\appendix
\renewcommand{\sectionmark}[1]{\markboth{\MakeUppercase{%
\appendixname\ \thesection}} {\MakeUppercase{#1}} }

\section{Material de apoio}

Alguns materiais de apoio coletados para este trabalho,
em nenhuma ordem específica. Consulte com alguma cautela.

\begin{itemize}
\item \textsc{Allison}, Dale C.
\href{https://biblicalstudies.org.uk/pdf/jbl/1987_allison.pdf}
{The Structure of the Sermon on the Mount}.
\textbf{Journal of Biblical Literature}, v. 106, n. 3, p. 423-445, 1987.

\item \textsc{Brooks}, James A. \href{http://faculty.gordon.edu/hu/bi/ted_hildebrandt/NTeSources/NTArticles/CTR-NT/Brooks-UnitySeronMt-CTR.pdf}
{The Unity and Structure of the Sermon on the Mount}.
\textbf{Criswell Theological Review}, v. 6, n. 1, p. 15-28, 1992.

\item \textsc{Lindberg}, Tod. What the Beatitudes Teach.
\textbf{Policy Review}, 30 jul. 2007. Disponível em:
\textless\url{https://www.hoover.org/research/what-beatitudes-teach}\textgreater.

\item \textsc{Pennington}, Jonathan. 3 Things You Didn't Know About the Sermon
on the Mount. \textbf{The Gospel Coalition}, 16 nov. 2017. Disponível em:
\textless\url{https://www.thegospelcoalition.org/article/3-things-didnt-know-sermon-mount/}\textgreater.

\item \textsc{Mahoney SJ}, John A. The Meaning of the Sermon on the Mount.
\textbf{Thinking Faith}, 29 mai. 2008. Disponível em:
\textless\url{https://www.thinkingfaith.org/articles/20080529_1.htm}\textgreater.

\item \underline{\hspace{1.5cm}}. The Shape of the Sermon on the Mount.
\underline{\hspace{1.5cm}}, 13 jul. 2011. Disponível em:
\textless\url{https://www.thinkingfaith.org/articles/20110713_1.htm}\textgreater.

\item \underline{\hspace{1.5cm}}. Living the Sermon on the Mount.
\underline{\hspace{1.5cm}}, 20 jul. 2011. Disponível em:
\textless\url{https://www.thinkingfaith.org/articles/20110720_1.htm}\textgreater.

\item \textsc{Dornelas}, Sabrina R. de S.
\href{http://www.filologia.org.br/viiicnlf/anais/caderno13-05.html}
{A Mimese no Sermão da Montanha}.
\textbf{Cadernos do Congresso Nacional de Lingüística e Filologia}, Série VIII,
n. 13, ago. 2004.

\item \textsc{Cranford}, Lorin L. \textbf{The Literary Structure of
the Sermon on the Mount}. Disponível em:
\textless\url{https://cranfordville.com/sermt-ot.pdf}\textgreater.

\item \textsc{StackExchange}. What is the framework for the structure of
the Sermon on the Mount?. \textbf{Biblical Hermeneutics StackExchange},
28 nov. 2015. Disponível em:
\textless\url{https://hermeneutics.stackexchange.com/questions/20794/what-is-the-framework-for-the-structure-of-the-sermon-on-the-mount}\textgreater.

\item \textsc{Betz}, Hans D. The Sermon on the Mount: Its Literary Genre and
Function. \textbf{The Journal of Religion}, v. 59, n. 3, p. 285-297, 1979.

\item \textsc{Kingsbury}, Jack D. The Place, Structure, and Meaning of the
Sermon on the Mount Within Matthew. \textbf{Interpretation}, v. 41, n. 2,
p. 131-143, 1987.

\end{itemize}

\end{document}
